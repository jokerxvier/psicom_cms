@extends('layouts.app')

@section('content')
	<div class="container">	
		

    	<div class="row" style="margin-top : 40px;">
        	<div class="col-md-6 col-md-offset-3">
        		@component('components.successMessage') @endcomponent
				@component('components.errorMessage')
		            This is the alert message here.
		        @endcomponent
        		 <div class="portlet light bordered">
	                <div class="portlet-title">
	                    <div class="caption font-green">
	                        <i class="icon-pin  font-green"></i>
	                        <span class="caption-subject bold uppercase"> Send Invites</span>
	                    </div>
	                </div><!--end title-->

	                 <div class="portlet-body form">
	                 	<form action="{{ route('invite.send') }}" method="post">
						    {{ csrf_field() }}
						    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} form-md-line-input">
						    	<input type="text" class="form-control" id="form_control_1" placeholder="Enter email" name="email">
                        		<label for="form_control_1">Email</label>
						    </div>

						    <div class="form-actions noborder">
                        		<button type="submit" class="btn green btn-block btn-lg">Send Invite</button>
                    		</div>

						   
						</form>

	                 </div>
	            </div>
        	</div>
        </div>
		
	</div><!--end container-fluid-->

@endsection


