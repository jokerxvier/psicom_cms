@extends('layouts.app')

@section('content')
<div class="container">
    <div class="logo">
        <img src="{{asset('images/psicom_author_logo.png')}}" alt=""> 
    </div><!--end logo-->
    <div class="row">
        <div class="col-md-6 col-md-offset-3">

            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="icon-pin  font-green"></i>
                        <span class="caption-subject bold uppercase"> Forgot Password</span>
                    </div>
                </div><!--end title-->

                <div class="portlet-body form">
                    {!! Form::open(['url' =>  route('password.email'), 'class' => 'formDisableOnSubmit' ]) !!}

                        {{ csrf_field() }}

                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} form-md-line-input">
                            <input type="text" class="form-control" id="form_control_1" placeholder="Enter your name" name="email">
                            <label for="form_control_1">Email</label>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                           
                        </div>

                         <div class="form-actions noborder">
                            <button type="submit" class="btn green btn-block btn-lg">Forgot Password</button>
                        </div>


                     {!! Form::close() !!}
                </div><!--end portlet-body-->
            </div><!--end portlet-body-->

        </div>
    </div>
</div>
@endsection
