@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">


            <div class="logo">
                <img src="{{asset('images/psicom_author_logo.png')}}" alt=""> 
            </div><!--end logo-->

            @component('components.successMessage')
                This is the alert message here.
            @endcomponent

            @component('components.errorMessage')
                This is the alert message here.
            @endcomponent

            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="icon-pin  font-green"></i>
                        <span class="caption-subject bold uppercase"> Reset Password</span>
                    </div>
                </div><!--end title-->
                
                {!! Form::open(['url' =>  route('password.reset.post'), 'class' => 'formDisableOnSubmit' ]) !!}
                    <div class="form-container">
                            <input type="hidden" name="token" value="{{ $token }}">

                           
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} form-md-line-input">
                                <input type="text" class="form-control" id="form_control_1" placeholder="Enter your name" name="email">
                                <label for="form_control_1">Email</label>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} form-md-line-input">
                                <input type="password" class="form-control" id="form_control_1" placeholder="Enter your password" name="password">
                                <label for="form_control_1">Password</label>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                               
                            </div>

                            <div class="form-group {{ $errors->has('password_confirm') ? ' has-error' : '' }}  form-md-line-input">
                                <input type="password" class="form-control" id="form_control_1" placeholder="Enter your password" name="password_confirmation">
                                <label for="form_control_1">Confirm Password</label>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                               
                            </div>

                            <div class="form-actions noborder">
                                <button type="submit" class="btn green btn-block btn-lg">Reset Password</button>
                            </div>
                    </div><!--form-container-->
                {!! Form::close() !!}
            </div><!--end portlet-->

        </div><!--end col-->
    </div>
</div>
@endsection
