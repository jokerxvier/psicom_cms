@extends('layouts.app')

@section('content')
<div class="container">
    <div class="logo">
        <img src="{{asset('images/psicom_author_logo.png')}}" alt=""> 
    </div><!--end logo-->
    <div class="row" style="margin-top : 40px;">
        <div class="col-md-6 col-md-offset-3">
            @component('components.errorMessage')
                This is the alert message here.
            @endcomponent

           <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="icon-pin  font-green"></i>
                        <span class="caption-subject bold uppercase"> Register</span>
                    </div>
                </div><!--end title-->

                <div class="portlet-body form">

                {!! Form::open(['url' =>  route('auth.register'), 'class' => 'formDisableOnSubmit' ]) !!}

                    {{Form::hidden('token', $token)}}

                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} form-md-line-input">
                        <input type="text" class="form-control" id="form_control_1" placeholder="Enter your name" name="name" value="{{ old('name') }}">
                        <label for="form_control_1">Name</label>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                       
                    </div>


                    <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }} form-md-line-input">
                        <input type="text" class="form-control" id="form_control_1" placeholder="Enter your username" name="username" value="{{ old('username') }}">
                        <label for="form_control_1">Username</label>
                      

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}  form-md-line-input">
                        <input type="text" class="form-control" id="form_control_1" placeholder="Enter your email" name="email" value="{{ old('email') }}">
                        <label for="form_control_1">Email</label>
                  

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} form-md-line-input">
                        <input type="password" class="form-control" id="form_control_1" placeholder="Enter your password" name="password">
                        <label for="form_control_1">Password</label>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                       
                    </div>

                    <div class="form-group {{ $errors->has('password_confirm') ? ' has-error' : '' }}  form-md-line-input">
                        <input type="password" class="form-control" id="form_control_1" placeholder="Enter your password" name="password_confirm">
                        <label for="form_control_1">Confirm Password</label>

                        @if ($errors->has('password_confirm'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirm') }}</strong>
                            </span>
                        @endif
                       
                    </div>


                    <div class="form-actions noborder">
                        <button type="submit" class="btn green btn-block btn-lg">Register</button>
                    </div>

                {!! Form::close() !!}

                </div><!--end portlet-body-->
           </div><!--end portlet-->
        </div>
    </div>
</div>
@endsection
