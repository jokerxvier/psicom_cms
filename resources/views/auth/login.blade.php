@extends('layouts.app')

@section('content')
<div class="container">
    <div class="logo">
        <img src="{{asset('images/psicom_author_logo.png')}}" alt="">
    </div><!--end logo-->

   
    

    <div class="content">


        <h3 class="form-title font-green">Sign In</h3>

        @component('components.errorMessage')
       
            This is the alert message here.
         @endcomponent

         @component('components.successMessage')
       
            This is the alert message here.
         @endcomponent

        {!! Form::open(['url' =>  route('auth.login'), 'class' => 'formDisableOnSubmit' ]) !!}
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="control-label visible-ie8 visible-ie9">Username</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div><!--end form-group-->

            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"> 
            </div><!--end form-group-->

            <div class="form-actions">
                <button type="submit" class="btn green uppercase">Login</button>
                <!--<label class="rememberme check mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" name="remember" value="1">Remember
                    <span></span>
                </label>-->
                <a href="{{URL::route('password.reset.get')}}" id="forget-password" class="forget-password">Forgot Password?</a>
            </div>

                <div class="create-account">
                   {{-- <p>
                        <a href="{{route('register')}}" id="register-btn" class="uppercase">Create an account</a>
                    </p>--}}
            </div>
        {!! Form::close() !!}

    </div><!--end content-->
</div>
@endsection
