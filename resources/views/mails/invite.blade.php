@extends('beautymail::templates.widgets')

@section('content')

	@include('beautymail::templates.widgets.articleStart')

		<h4 class="secondary"><strong>You have been invited to become PSICOM author</strong></h4>
		<p>Use the the following link to register an account</p>

		<p style="text-align: center;">
			<a target="_blank" href="{{route('register', ['token' => $invite->token])}}" class="button button-blue" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; border-radius: 3px; box-shadow: 0 2px 3px rgba(0, 0, 0, 0.16); color: #FFF; display: inline-block; text-decoration: none; -webkit-text-size-adjust: none; background-color: #3097D1; border-top: 10px solid #3097D1; border-right: 18px solid #3097D1; border-bottom: 10px solid #3097D1; border-left: 18px solid #3097D1;">
					Register Account
			</a>
		</p>

		

	@include('beautymail::templates.widgets.articleEnd')




@stop