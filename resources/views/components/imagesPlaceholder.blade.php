
<img class="lazy {{$class or ''}}"  data-src="{{$src}}" width="{{$width}}" height="{{$height}}" />

{{--<img src="{{$src}}" width="{{$width}}" height="{{$height}}"  class="{{$class or ''}}">--}}

<noscript>
    <img src="{{$src}}" width="{{$width}}" height="{{$height}}"  class="{{$class or ''}}">
</noscript>