<div class="modal fade" tabindex="-1" role="dialog" id="termsModal"> 
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Terms and Conditions</h4>
      </div>
      <div class="modal-body" style="height: 250px;overflow-y: auto;">
        <div class="entry-post">
         
            <p>This confirms your commitment to assign and deliver exclusively to PSICOM Publishing Inc, (hereafter, “PSICOM”),with a fully edited, proofread and final manuscript of the Work, formatted and edited in accordance with the guidelines for manuscripts provided by <strong>PSICOM</strong>. <strong>PSICOM</strong> is not obligated to further edit or reformat the Work.</p>
            <p>1. Term. This Agreement shall be valid and effective for a period of five (5) years, From the date your work is published on the app, renewable for the same period at the option of PSICOM.</p>
            <p>2. The Author warrants that he/she is the exclusive owner of said Work, and has the legal authority to enter into this agreement and grant publishing rights regarding the Work. Furthermore, the Author states that the Work is free of any counts of libel, copyright infringement, plagiarism, misrepresentation of facts or breach of privacy. The Author also assures PSICOM that the Work (including all submitted artwork) is completely original (or that the Author has license to use such content), is not in public domain and has not been published in paperback or electronic form under a contract that would conflict with this Agreement. The Author also agrees that he/she is the sole owner of the copyrights of the Work. The Author agrees not to enter any agreement with any person, firm or corporate entity that would conflict with the rights hereby granted to PSICOM without first terminating this contract. <strong>&nbsp;</strong>The Author also agrees that he/she will hold PSICOM <strong>&nbsp;harmless</strong> against any recovery or penalty arising out of his/her breach of this warranty.</p>
            <p>3. The Author releases PSICOM from any legal actions that may arise from the Work. The Author will hold PSICOM harmless against legal actions that may arise from plagiarism, breach of privacy, misrepresentation of facts, the authenticity of the Work or copyright infringement. The Author will also pay any and all legal fees and judgments that result from such a suit. <strong>In turn, PSICOM assures the Author that any and all artwork used by the Publisher (which was not submitted by the Author) is free of copyright infringement.</strong></p>
           
            <p>4. The Author understands and gives permission for PSICOM to promote and sell the Author’s eBook worldwide. The Author also understands if his or her contract is terminated, whether by PSICOM or by his<strong>/her</strong> own will, that it may take up to six (6) month for the Author’s eBook to be removed from the database of PSICOM.</p>
            <p>5. The Author agrees to furnish PSICOM with any materials requested for publicity purposes, including reviews, photos, summaries, etc. The Author also agrees that PSICOM has the right to use his/her name and likeness for any and all promotional purposes during the term of this contract. PSICOM also has the full right to use excerpts, quotes, reviews, etc. from the work for publicity purposes.</p>
            <p>6. On all sales of eBook or Work, PSICOM will pay the Author a royalty equal to 40% of the payments actually received from sales of eBook or Work, less any <strong>discounts</strong>, <strong>taxes</strong>, and returns.</p>
            <p>7. <strong>Royalty payment and royalty reports are generated after 30 days every quarter ending March, June, September, December of each fiscal year.&nbsp;A minimum threshold of at least Php 2,000.00 has to be reached for payment to be remitted to the AUTHOR. If royalty is below the threshold the amount will be carried forth to the subsequent quarter until it will reach the threshold.</strong></p>
            <p>If no sales are recorded, the Author understands that he or she will not receive a royalty check or royalty report. The Author also understands that royalty reports are available at <strong>any time</strong>, regardless of whether there have been sales or not, and can be requested by e-mailing royalties@psicompublishing.com.</p>
            <p>8. This Agreement shall be valid and binding upon execution hereof. A long form agreement between you and PSICOM shall be executed subsequently to clarify the terms herein, if necessary.</p>

            <p>If the foregoing faithfully reflects our discussions, kindly signify your conformity by signing on the space hereunder.</p>
    
        
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-accept">Accept</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->