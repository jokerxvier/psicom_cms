<div id="confirm" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-body">
        <p>Are you sure?</p>
    </div>
    <div class="modal-footer">
    	<button type="button" data-dismiss="modal" class="btn green"  id="yes">Yes</button>
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
        
    </div>
</div>
