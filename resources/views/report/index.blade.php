@extends('layouts.app')

@section('content')
	<div class="container-fluid">	
		
		<div class="page-content">

			 @component('components.errorMessage')
        			This is the alert message here.
     		 @endcomponent

     		 @component('components.successMessage')
        			This is the alert message here.
     		 @endcomponent


     		
			<div class="col-md-12">
				@component('components.search', ['form_url' => route('promo')] ) @endcomponent
			</div> <!--end col-md-12-->

			<div class="page-content-row">
				


				<div class="page-content-col">
					
				

					<div class="portlet light portlet-fit bordered">
						<div class="portlet-title">
	                        <div class="caption">
	                            <i class="icon-settings font-blue"></i>
	                            <span class="caption-subject font-blue sbold uppercase">Manage Reports</span>
	                        </div><!--end caption-->

	                        
	                        
	                    </div><!--end portlet-title-->

	                    <div class="portlet-body">
	                    	<div class="table-scrollable-borderless">
	                    		<table class="table table-hover table-light">
	                    			<thead>
	                            		<tr class="uppercase">
		                            		<td></td>
		                            		<td>Title</td>
		                            		<td>Message</td>
		                            		<td>Report By</td>
		                            		<td>Book Name</td>
		                            		<td>Creared at</td>
		                            		<td>Actions</td>
		                            	</tr>
	                            	</thead>

	                            	<tbody>

	                            		@foreach($reports as $report)

		                            		<tr>
		                            			
		                            			<td>{{$report->title}}</td>
		                            			<td>{{$report->message}}</td>
		                            			<td>{{ $report->user->name }}</td>
		                            			<td>{{  $report->book->title }}</td>
		                            			<td>{{$promo->membership->title}}</td>
		                            			<td></td>
		                            			<td class="clearfix">

		                            				<a href="{{route('promo.dashboard', $promo->id)}}" class="btn btn-circle blue btn-outline btn-block  margin-bottom-10">Dashboard</a>

		                            				@if (Route::currentRouteName() != 'promo.expired')

			                            				{!! Form::open(['route' => ['promo.set.featured', $promo->id], 'method' => 'POST', 'onsubmit' => 'return confirm("are you sure ?")']) !!}

			                            					@if (!$promo->featured)
			                            						<button type="submit" class="btn btn-circle green btn-outline btn-block  margin-bottom-10">Set as Featured</button>
			                            				  @endif

			                            				  @if ($promo->featured)
			                            						<button type="submit" class="btn btn-circle green  btn-block  margin-bottom-10">Remove as Featured</button>
			                            				  @endif

			                            				{!! Form::close() !!}
			                            			@endif

		                            				{!! Form::open(['route' => ['promo.delete', $promo->id], 'method' => 'DELETE', 'onsubmit' => 'return confirm("are you sure ?")']) !!}
					  	                             {{ method_field('DELETE') }}

					  	                             	<button type="submit" class="btn btn-circle red btn-outline btn-block  margin-bottom-10">Delete</button>
					                              	{!! Form::close() !!}
		                            			</td>
		                            		</tr>
		                            	@endforeach
	                            		

	                            	</tbody>

	                    		</table>
	                    	</div><!--end table-scrollable-borderless-->

	                    	<div class="row">
                    			@component('components.paginate', ['data' => $promos]) @endcomponent
                    		</div><!--end row-->
	                    </div><!--end portlet-body-->


					</div><!--end portlet-->
				</div><!--end col-md-12-->
			</div><!--end page-content-row-->
		</div><!--end page-content-->

	</div><!--end container-fluid-->

@endsection



@section('style')
	<link href="{{ asset('themes/pages/css/search.min.css') }}" rel="stylesheet" type="text/css" />

@stop