{!! Form::open(['route' => ['book.update', $book->id], 'method' => 'POST', 'files' => true, 'class' => 'formControl']) !!}
	<div class="row">
		<div style="padding : 10px">
			@component('components.infoMessage')
		        @slot('message')
		            Must be in PNG,  or JPG format, smaller than 2MB. Recommended cover dimensions: 288 x 450 pixels
		        @endslot
		    @endcomponent
	    </div>


		<div class="col-md-4">
			<div class="form-group">
				<div class="fileinput fileinput-new" data-provides="fileinput" style="margin : 0pt auto">
	                <div class="fileinput-new thumbnail" style="width: 288px; height: 450px;">
	                	@if($book->images->count() > 0)
	                		@component('components.imagesPlaceholder', ['src' => $book->images->first()->image_path, 'width' => '288', 'height' => '450'])
                            @endcomponent
	                    	
	                    @else 
	                    	<img src="https://s3.amazonaws.com/psicom/defaul_cover.png" alt="" /> 
	                   	@endif
	                    
	                </div>
	                <div class="fileinput-preview fileinput-exists thumbnail" style="width: 288px; height: 450px;"> </div>
	                <div style="text-align: center;">
	                    <span class="btn default btn-file">
	                        <span class="fileinput-new"> Upload Cover </span>
	                        <span class="fileinput-exists"> Change </span>
	                        <input type="file" name="cover"> </span>
	                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
	                </div>
	            </div>
	        </div><!--end form-group-->
		</div><!--end col-md-4-->
		
		<div class="col-md-8 ">

			<div class="portlet light tasks-widget bordered">

			
					<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }} form-md-line-input">
			            {{ Form::input('text', 'title', $book->title, ['class' => 'form-control', 'placeholder' => 'Enter book title']) }}

			            <label for="form_control_1">Title</label>

			            @if ($errors->has('title'))
			                <span class="help-block">
			                    <strong>{{ $errors->first('title') }}</strong>
			                </span>
			            @endif
			           
			        </div><!--end form-group-->


			        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }} form-group form-md-line-input">
			           

			        {{Form::textarea('description', $book->description, ['placeholder' => 'Write a short description', 'class' => 'form-control', 'rows' => 4])}}

			            <label for="form_control_1">Book Summary</label>

			            @if ($errors->has('description'))
			                <span class="help-block">
			                    <strong>{{ $errors->first('description') }}</strong>
			                </span>
			            @endif
			           
			        </div><!--end form-group-->

			        <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }} form-group form-md-line-input">
			         
			           {{-- Form::select('category', $categories, $book_category_id,['class' => 'form-control', 'placeholder' => 'Select Category']) --}}

			            {!! Form::select('category[]', $categories, $book_category_id, ['multiple' => true, 'class' => 'form-control margin mobileSelect']) !!}

			            <label for="form_control_1">Category</label>

			            @if ($errors->has('category'))
			                <span class="help-block">
			                    <strong>{{ $errors->first('category') }}</strong>
			                </span>
			            @endif
			           
			        </div><!--end form-group-->


			        <div class="form-group {{ $errors->has('release_date') ? ' has-error' : '' }} form-group form-md-line-input">
			         
			            {{ Form::input('text', 'release_date', $book->release_date->format('Y-m-d'), ['class' => 'form-control date-picker' , 'data-date-format' => "yyyy-mm-dd"]) }}

			            <label for="form_control_1">Release Date</label>

			            @if ($errors->has('release_date'))
			                <span class="help-block">
			                    <strong>{{ $errors->first('release_date') }}</strong>
			                </span>
			            @endif
			           
			        </div><!--end form-group-->

			       

			        <div class="form-group form-md-radios">

			        	<label for="form_control_1">Visibility</label>
	                                            
	                    <div class="md-radio-inline">
	                        <div class="md-radio">
	                            {{Form::radio('visibility', 'published', ($book->visibility == 'published') ?  $book->visibility : null, ['class' => 'md-radiobtn', 'id' => 'radio6'])}}

	                            <label for="radio6">
	                                <span></span>
	                                <span class="check"></span>
	                                <span class="box"></span> Published </label>
	                        </div>
	                        <div class="md-radio">
	                        	{{Form::radio('visibility', 'unpublished', ($book->visibility == 'unpublished') ?  $book->visibility : null,['class' => 'md-radiobtn', 'id' => 'radio7'])}}
	                            <label for="radio7">
	                                <span></span>
	                                <span class="check"></span>
	                                <span class="box"></span> Unpublished</label>
	                        </div>
	                        
	                    </div>
	                </div>

	                <div class="form-group form-md-radios">
	                	<label for="form_control_1">Ratings</label>

	                	<div class="md-radio-inline">
	                            <div class="md-radio">
	                            	{{Form::radio('ratings', 'general', ($book->ratings == 'general') ?  $book->ratings : null,['class' => 'md-radiobtn', 'id' => 'radio8'])}}
	                            
	                                <label for="radio8">
	                                    <span></span>
	                                    <span class="check"></span>
	                                    <span class="box"></span> General </label>
	                            </div>
	                            <div class="md-radio">
	                            	{{Form::radio('ratings', 'mature', ($book->ratings == 'mature') ?  $book->ratings : null,['class' => 'md-radiobtn', 'id' => 'radio9'])}}
	                    
	                                <label for="radio9">
	                                    <span></span>
	                                    <span class="check"></span>
	                                    <span class="box"></span> Mature</label>
	                            </div>
	                            
	                    </div>

	                </div><!--end form-group-->



				<div class="form-actions noborder">
	           		 <button type="submit" class="btn green btn-block btn-lg">Update</button>
	        	</div>

			</div><!--end light-->
			

		</div><!--end col-md-8-->
	</div>
{!! Form::close() !!}

@section('style')
	<link rel="stylesheet" href="{{asset('themes/global/plugins/multi-select/bootstrap-fullscreen-select.css')}}">
@stop

@section('script')
	{{ Html::script('themes/global/plugins/multi-select/bootstrap-fullscreen-select.js') }}     
	{{ Html::script('themes/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}    
	<script type="text/javascript">

	    var isBookFree = "{{$book->membership_type}}";

	    if (isBookFree == 3)
	    {
	    	$('select[name="points"]').hide();
	    }


		$(function () {
			 $('.mobileSelect').mobileSelect();
		    $('.select-membership').on('change', function () {
		       var memberShipValue = $(this).val();

		       if (memberShipValue == 3)
		        {
		        	$('.form-points').hide();
		        	$('select[name="points"]').hide();
		        }else {
		        	$('.form-points').show();
		        	$('select[name="points"]').show();
		        }
		    });
		});  
	</script>
@stop