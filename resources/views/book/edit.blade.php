@extends('layouts.app')

@section('content')
	<div class="container-fluid book-page">	

		<div class="page-content">
			@component('components.successMessage') @endcomponent
			@component('components.errorMessage')
	            This is the alert message here.
	        @endcomponent
			<div class="page-content-row">
				
				@component('book.bookSidebar', ['book_id' => $book->id, 'book' => $book]) @endcomponent

				<div class="page-content-col">
					@component('book.bookInfoComp', ['book' => $book, 'categories' => $categories, 'book_category_id' => $book_category_id, 'membership' => $membership, 'points' => $points]) @endcomponent
				</div><!--end page content-col-->
			</div><!--end page-content-row-->

		
		</div><!--end page-content-->

	</div><!--end container-fluid-->

@endsection

@section('script')
	{{ Html::script('themes/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }} 

@stop