@extends('layouts.app')

@section('content')

	<div class="container-fluid book-page">	

		<div class="page-content">
		@component('components.successMessage') @endcomponent
		@component('components.errorMessage')
            This is the alert message here.
        @endcomponent
			<div class="page-content-row">
				@component('book.bookSidebar', ['book_id' => $book->id, 'book' => $book]) @endcomponent

				<div class="page-content-col">

                   
                   
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Update PDF</span>
                                </div>

                             </div>

                           <div class="portlet-body">

                                 @component('components.infoMessage')
                                    @slot('message')
                                        Must be a pdf file format. 
                                    @endslot
                                @endcomponent

                                <div class="row">
                                    <div class="col-md-4 col-md-offset-4" style="text-align: center;"> 
                             
                                        {!! Form::open(['url' =>  route('book.pdf.update', [$book->id, $chapters->id]), 'class' => 'formDisableOnSubmit', 'files' => true ]) !!}
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new pdfFile" style="width: 200px; height: 200px;">
                                                      @component('components.imagesPlaceholder', ['src' => asset('images/pdf-icon.svg'), 'width' => '200', 'height' => '200'])

                                                      @endcomponent

                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;"> </div>
                                                    <div class="col-md-12 text-center">
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Update Pdf File </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="pdfFile"> 
                                                            </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class=" col-md-12 margin-top-10 text-center">
                                                <div class="form-body">
                                                    <div class="form-group form-md-line-input">
                                                            <input type="text" class="form-control" id="form_control_1" placeholder="Enter Amount" style="text-align: center;" name="amount" value="{{$chapters->points}}">
                                                            <label for="form_control_1">Set Amount</label>
                                                            <span class="help-block">Amount should be between 0 - 200</span>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class=" col-md-12 margin-top-10 text-center">
                                                <button  type="submit" class="btn green btn-block"> Save Pdf </button>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div><!--end row-->
                 
                           </div><!--end portlet-body-->
                        </div><!--end portlet-->
                 


					
				</div><!--end page content-col-->
			</div><!--end page-content-row-->

		
		</div><!--end page-content-->

	</div><!--end container-fluid-->

@endsection

@section('script')
	{{ Html::script('themes/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }} 

    <script type="text/javascript">
        $( function() {
            var url = "{{URL::route('chapter.sortable', $book->id)}}"
            $( "#sortable" ).sortable({
                handle : '.handle', 
                update : function () { 
                    var order = $('#sortable').sortable('serialize'); 
                    axios.post(url, order)
                    .then(function (response) {

                        if(!response.data.success) {
                            console.log('Whoops, something went wrong :/');
                        }
            
                    })
                    .catch(function (error) {
                        if (error)
                        {
                            console.log('Whoops, something went wrong :/');
                        }
                    });
                }
            });
 
        });
    </script>

@stop