@extends('layouts.app')

@section('content')
	<div class="container-fluid book-page">	

		<div class="page-content">
		@component('components.successMessage') @endcomponent
		@component('components.errorMessage')
            This is the alert message here.
        @endcomponent


        @component('components.infoMessage')
	        @slot('message')
	            Must be in PNG,  or JPG format, smaller than 2MB. Recommended cover dimensions: 288 x 450 pixels
	        @endslot
	    @endcomponent

		{!! Form::open(['route' => 'book.insert', 'method' => 'POST', 'files' => true, 'class' => 'formControl formDisableOnSubmit', 'id' => 'createBooksForm']) !!}
			<div class="row">
				<div class="col-md-4" style="width : 28% !important">
					
					<div class="form-group">
						<div class="fileinput fileinput-new" data-provides="fileinput" style="margin : 0pt auto">
			                <div class="fileinput-new thumbnail">
			                    <img src="http://www.placehold.it/288x450/EFEFEF/AAAAAA" alt="" /> 
			                </div>
			                <div class="fileinput-preview fileinput-exists thumbnail" style="width: 288px; height: 450px;"> </div>
			                <div style="text-align: center;">
			                    <span class="btn default btn-file">
			                        <span class="fileinput-new"> Upload Cover </span>
			                        <span class="fileinput-exists"> Change </span>
			                        <input type="file" name="cover"> </span>
			                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
			                </div>
			            </div>
			        </div><!--end form-group-->
				</div><!--end col-md-4-->
				
				<div class="col-md-8 ">


					<div class="portlet light tasks-widget bordered">

					
							<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }} form-md-line-input">
					            {{ Form::input('text', 'title', old('title'), ['class' => 'form-control', 'placeholder' => 'Enter book title']) }}

					            <label for="form_control_1">Title</label>

					            @if ($errors->has('title'))
					                <span class="help-block">
					                    <strong>{{ $errors->first('title') }}</strong>
					                </span>
					            @endif
					           
					        </div><!--end form-group-->


					        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }} form-group form-md-line-input">
					           

					        {{Form::textarea('description', old('description'), ['placeholder' => 'Write a short description', 'class' => 'form-control', 'rows' => 4])}}

					            <label for="form_control_1">Book Summary</label>

					            @if ($errors->has('description'))
					                <span class="help-block">
					                    <strong>{{ $errors->first('description') }}</strong>
					                </span>
					            @endif
					           
					        </div><!--end form-group-->

					        <div class="form-group {{ $errors->has('category') ? ' has-error' : '' }} form-group form-md-line-input">
					         
					           {{-- Form::select('category', $categories, old('category'),['class' => 'form-control', 'placeholder' => 'Select Category'])  --}}


					           {!! Form::select('category[]', $categories, null, ['multiple' => true, 'class' => 'form-control margin mobileSelect']) !!}
					       


					            <label for="form_control_1">Category</label>

					            @if ($errors->has('description'))
					                <span class="help-block">
					                    <strong>{{ $errors->first('category') }}</strong>
					                </span>
					            @endif
					           
					        </div><!--end form-group-->

					        

					       

					        <div class="form-group {{ $errors->has('release_date') ? ' has-error' : '' }} form-group form-md-line-input">
					         
					            {{ Form::input('text', 'release_date', old('release_date'), ['class' => 'form-control date-picker' , 'data-date-format' => "yyyy-mm-dd"]) }}

					            <label for="form_control_1">Release Date</label>

					            @if ($errors->has('release_date'))
					                <span class="help-block">
					                    <strong>{{ $errors->first('release_date') }}</strong>
					                </span>
					            @endif
					           
					        </div><!--end form-group-->


                            <div class="form-group form-md-radios">
                            	<label for="form_control_1">Ratings</label>

                            	<div class="md-radio-inline">
                                        <div class="md-radio">
                                        	 {{ Form::radio('ratings', 'general', null ,['class' => 'md-radiobtn', 'id' => 'radio8']) }}

                                            
                                            <label for="radio8">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> General </label>
                                        </div>
                                        <div class="md-radio">
                                        	{{ Form::radio('ratings', 'mature', null ,['class' => 'md-radiobtn', 'id' => 'radio9']) }}
                                            
                                            <label for="radio9">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Mature</label>
                                        </div>
                                        
                                </div>

                            </div><!--end form-group-->


                            <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }} form-group form-md-line-input">
					         
					           {!! Form::select('type', ['book' => 'Book', 'pdf' => 'Pdf'], old('type'),['class' => 'form-control', 'placeholder' => 'Select Book Format']) !!}

					            <label for="form_control_1">Book Format</label>

					            @if ($errors->has('type'))
					                <span class="help-block">
					                    <strong>{{ $errors->first('type') }}</strong>
					                </span>
					            @endif
					           
					        </div><!--end form-group-->



						<div class="form-actions noborder">
							<div class="row">
								<div class="col-md-6">
									<div class="mt-checkbox-list">
										<label class="mt-checkbox">
											{{ Form::checkbox('terms', null,null) }}
											I’ve read and accept the <a href="#" class="termsAndCondition">Terms and Conditions.</a>
											<span></span>
										</label>
									
									</div>
								</div>
								<div class="col-md-6">
									<div class="btn-group pull-right">
							 			<a href="{{URL::route('books')}}" class="btn red btn-lg margin-right-10" >Cancel</a>
                       			 		<button type="submit" class="btn green btn-lg ">Create Book</button>
                    				</div>
								</div>
							</div>

							

                    		<div class="clearfix"></div>
                    	</div>

					</div><!--end light-->
					

				</div><!--end col-md-8-->
			</div>

		{!! Form::close() !!}




		</div><!--end page-content-->

	</div><!--end container-fluid-->

	@component('components.termsModal') @endcomponent

@endsection

@section('style')
	<link rel="stylesheet" href="{{asset('themes/global/plugins/multi-select/bootstrap-fullscreen-select.css')}}">
@stop

@section('script')
	{{ Html::script('themes/global/plugins/multi-select/bootstrap-fullscreen-select.js') }}   
	{{ Html::script('themes/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}    
	{{ Html::script('js/books.js') }}  


	<script type="text/javascript">
		$(function () {

			$(function () {
                $('.mobileSelect').mobileSelect();
            });

		    $('.select-membership').on('change', function () {
		       var memberShipValue = $(this).val();

		       if (memberShipValue == 3)
		        {
		        	$('.form-points').hide();
		        }else {
		        	$('.form-points').show();
		        }
		    });
		});  


	</script>

@stop