@extends('layouts.app')

@section('content')

	<div class="container-fluid book-page">	

		<div class="page-content">
            <div style="padding : 10px">
                @component('components.infoMessage')
                    @slot('message')
                        Author cannot delete the chapters if there are already users who bought it.
                    @endslot
                @endcomponent
            </div>


    		@component('components.successMessage') @endcomponent
    		@component('components.errorMessage')
                This is the alert message here.
            @endcomponent
			<div class="page-content-row">
				@component('book.bookSidebar', ['book_id' => $book->id, 'book' => $book]) @endcomponent

				<div class="page-content-col">

                   
                    @if($chapters->count() <= 0)
                        <div class="portlet light portlet-fit bordered">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject font-dark sbold uppercase">Upload PDF</span>
                                </div>

                             </div>

                           <div class="portlet-body">

                                 @component('components.infoMessage')
                                    @slot('message')
                                        Must be a pdf file format. 
                                    @endslot
                                @endcomponent

                                <div class="row">
                                    <div class="col-md-4 col-md-offset-4" style="text-align: center;"> 
                             
                                        {!! Form::open(['url' =>  route('book.pdf.create', $book->id), 'class' => 'formDisableOnSubmit', 'files' => true ]) !!}
                                            <div class="form-group">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new pdfFile" style="width: 200px; height: 200px;">
                                                      @component('components.imagesPlaceholder', ['src' => asset('images/pdf-icon.svg'), 'width' => '200', 'height' => '200'])

                                                      @endcomponent

                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;"> </div>
                                                    <div class="col-md-12 text-center">
                                                            <span class="btn default btn-file">
                                                                <span class="fileinput-new"> Select Pdf File </span>
                                                                <span class="fileinput-exists"> Change </span>
                                                                <input type="file" name="pdfFile"> 
                                                            </span>
                                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class=" col-md-12 margin-top-10 text-center">
                                                <div class="form-body">
                                                    <div class="form-group form-md-line-input">
                                                            <input type="text" class="form-control" id="form_control_1" placeholder="Enter Amount" style="text-align: center;" name="amount">
                                                            <label for="form_control_1">Set Amount</label>
                                                            <span class="help-block">Amount should be between 0 - 200</span>
                                                        </div>
                                                </div>
                                            </div>
                                            <div class=" col-md-12 margin-top-10 text-center">
                                                <button  type="submit" class="btn green btn-block"> Save Pdf </button>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div><!--end row-->
                 
                           </div><!--end portlet-body-->
                        </div><!--end portlet-->
                    @endif


					<div class="portlet light portlet-fit bordered">
					
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject font-dark sbold uppercase">Table of Contents</span>
                            </div>

                            
                        </div><!--end portlet-title-->

                        <div class="portlet-body">
                            @if ($chapters->count() > 0)
                            	<div class="table-scrollable-borderless">
                            		<table class="table table-hover table-light">
                            			<tbody  id="sortable">
                                            @foreach ($chapters as $chapter)
                                				<tr id="chapter-{{$chapter->id}}">
                                               
                         
                                                    <td> 
                                                    	<a href="{{$chapter->content}}" style="color: #444;"> {{$chapter->title}} </a><br />
                                                          
                                                    	<span style="color: #717171; font-size: 12px;"> {{$chapter->visibility}} | {{$chapter->created_at->format('M d Y')}} | People who bought : {{$chapter->inventory()->count()}} </span>
                                                    </td>

                                         
                                                    <td width="10%">
                                                        <div class="actions">
                                                            <div class="btn-group">
                                                                <a class="btn dark btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                                    <i class="fa fa-angle-down"></i>
                                                                </a>
                                                                <ul class="dropdown-menu pull-right">
                                                                    <li>
                                                                        <a href="{{$chapter->content}}" target="_blank"> View </a>
                                                                    </li>

                                                                    <li class="divider"> </li>
                                                                    <li>
                                                                        <a href="{{route('book.pdf.edit', [$book->id, $chapter->id])}}"> Edit </a>
                                                                    </li>

                                                                    <li class="divider"> </li>
                                                                    <li>

                                                                        {!! Form::open(['url' =>  route('book.pdf.publish', [$book->id, $chapter->id]), 'onsubmit' => 'return confirm("Are you sure ?")' ]) !!}
                                                                                <button type="submit" class="btn btn-link"> {{$chapter->visibility == 'published' ? 'Unpublish'  : 'Publish'}} </button>
                                                                        {!! Form::close() !!}
                                                                        
                                                                    </li>
                                                                    <li class="divider"> </li>
                                                                    <li>
                                                                        {!! Form::open(['url' =>  route('book.pdf.delete', [$book->id, $chapter->id]), 'onsubmit' => 'return confirm("Are you sure ?")' ]) !!}
                                                                                <button type="submit" class="btn btn-link"> Delete </button>
                                                                        {!! Form::close() !!}
                                                                    </li>
                                                                    
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach

                            			</tbody>
                            		</table>


                            	</div><!--end table-scrollable-->
                            @endif
                            @if ($chapters->count() <= 0)
                              <div class="col-md-12 page-500">
                              
                                  <div class=" details">
                                      <h3>You haven't written any stories yet.</h3>
                                    
                                      
                                  </div>
                              </div>

                              <div class="clearfix"></div>
                            @endif

                        </div><!--end portlet-body-->


                            
                

					</div><!--end portlet-->
				</div><!--end page content-col-->
			</div><!--end page-content-row-->

		
		</div><!--end page-content-->

	</div><!--end container-fluid-->

@endsection

@section('script')
	{{ Html::script('themes/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }} 

    <script type="text/javascript">
        $( function() {
            var url = "{{URL::route('chapter.sortable', $book->id)}}"
            $( "#sortable" ).sortable({
                handle : '.handle', 
                update : function () { 
                    var order = $('#sortable').sortable('serialize'); 
                    axios.post(url, order)
                    .then(function (response) {

                        if(!response.data.success) {
                            console.log('Whoops, something went wrong :/');
                        }
            
                    })
                    .catch(function (error) {
                        if (error)
                        {
                            console.log('Whoops, something went wrong :/');
                        }
                    });
                }
            });
 
        });
    </script>

@stop