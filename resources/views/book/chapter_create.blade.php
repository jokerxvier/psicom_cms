@extends('layouts.chapterLayout')

@section('content')
	@component('components.confirmModal') @endcomponent

	<header class="page-header">
		<nav class="navbar mega-menu" role="navigation">

			<div class="container-fluid">
				<div class="clearfix navbar-fixed-top">
					<a href="{{ url()->route('chapter', $book->id) }}" class="btn default btn-outline"><i class="fa fa-arrow-left"></i> Back</a>

					<div class="topbar-actions">
				
						@if ($chapter->visibility == 'unpublished')
							<button type="submit" id="publishButton" class="btn  btn-warning ladda-button" data-url="{{route('chapter.publish', [$book->id, $chapter->id])}}" data-style="expand-left" data-spinner-color="#333">Publish</button>
						@endif

						@if ($chapter->visibility == 'published')
							<button type="submit" id="publishButton" class="btn  btn-danger  ladda-button"  data-url="{{route('chapter.publish', [$book->id, $chapter->id])}}" data-style="expand-left">Unpublish</button>
						@endif

						<button type="button"  class="btn  btn-default ladda-button" data-style="expand-left" data-spinner-color="#333" id="saveButton" data-url="{{URL::route('chapter.update', [$book->id, $chapter->id])}}">Save</button>

					</div>


				</div><!--end navbar-->

				
			</div><!--<element></element>nd container-fluid-->

		</nav>
	</header>
	<div class="container-fluid book-page">	
		
			
		<div class="page-content">
			<div class="error alert alert-danger" style="display: none"> <strong>Whoops!</strong> There were some problems with your input.<br><br> <ul></ul> </div>
    		<div class="success alert alert-success" style="display: none"></div>

			<div class="writer">

				<h1 id="post-title" class="title-editable">{{$chapter->title}}</h1>
				<div class="row">
				<div class="col">
					<div class="col-md-6"><h2 style="text-align: center;">Set Chapter Amount</h2></div>
					<div class="col-md-6"><h3 id="post-amount" class="post-amount" style="border-bottom: 1px solid #ddd; text-align: center;">{{$chapter->points}}</h3></div>
				</div>
			</div>


				<div class="row">
				

				<div class="editable">
		            {!! $chapter->content !!}
	        	</div><!--end editable-->
	        	</div>
	        	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	        </div><!--writer-->
		</div><!--end page-content-->

	</div><!--end container-fluid book-page-->



	

@endsection

@section('script')
	
	<script type="text/javascript">
			$(document).bind('drop dragover', function (e) {
			     e.preventDefault();
			});
	</script>

	{{ Html::script('medium/js/medium-editor.min.js') }}
	{{ Html::script('medium/blueimp-file-upload/js/vendor/jquery.ui.widget.js') }}
	{{ Html::script('medium/blueimp-file-upload/js/jquery.iframe-transport.js') }}
	{{ Html::script('medium/blueimp-file-upload/js/jquery.fileupload.js') }}
	{{ Html::script('medium/handlebars/handlebars.runtime.min.js') }}
	{{--Html::script('medium/js/medium-editor-insert-plugin.js')--}}
	{{ Html::script('medium/jquery-sortable/source/js/jquery-sortable-min.js') }}

	{{ Html::script('medium/src/templates.js') }}
	{{ Html::script('medium/src/core.js') }}
	{{ Html::script('medium/src/images.js') }}





	<script type="text/javascript">


		var uploadPath = "{{ URL::route('chapter.upload', [$book->id]) }}";


		var titleEditor = new MediumEditor('.title-editable', {
		    buttonLabels: 'fontawesome',
		    toolbar: false
		});


		var AmountEditor = new MediumEditor('.post-amount', {
		    buttonLabels: 'fontawesome',
		    toolbar: false
		});

		var editor = new MediumEditor('.editable', {
            buttonLabels: 'fontawesome',
            embeds: false,
            toolbar: {
		        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3', 'quote', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull']
		    },
        });

        function handleUploadCompleted($el, data) {
        	$(':button').prop('disabled', false); 
			$('.medium-insert-active .loading').fadeOut('slow');   
		}

        $(function () {

        	axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        

	        $('.editable').mediumInsert({
	            editor: editor,

	            addons: {
                    images: {
                    	styles : false,
                    	preview: false,
                    	deleteScript: null, 
                    	fileUploadOptions: {
                    		url: uploadPath,
                            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                            dropZone : null,
                    
                    	},

                    	uploadCompleted: handleUploadCompleted,
                    	uploadFailed : function (uploadErrors, data) {
                    		console.log(uploadErrors);
                    	} 
                    },
                    embeds: false,
                }
	           
	        });

	        /*$('body').on('click', '#saveButton' ,function (e) {
	        	e.preventDefault();
					

	        	
				var l = Ladda.create(this);
	 			l.start();	  
	 			$(".btn").attr("disabled", true);
	 			

	        	var url = $(this).data('url');
	        	var postTitle = titleEditor.serialize();
	        	var postContent = editor.serialize();
	        	var amount = AmountEditor.serialize();

	        	
	        	
  				
  				axios.post(url, {
				    title: postTitle['post-title']['value']	,
				    amount : amount['amount-title']['value'],
				    content: postContent['element-0']['value']
				})
				.then(function (response) {

					if (response.data.success === false)
					{
						$('.error').html(response.data.message);
                		$('.error').fadeTo(2000, 500).slideUp(500, function(){
               				$(".error").slideUp(500);
                		});   
					}else {
						$('.success').html(response.data.message);
                		$('.success').fadeTo(2000, 500).slideUp(500, function(){
               				$(".success").slideUp(500);
                		});   
					}

					l.stop();
					$(".btn").attr("disabled", false);

				})
				.catch(function (error) {
				    l.stop();
				});
	        	
	        });*/

	        $('body').on('click', '#publishButton, #saveButton' ,function (e) {

	        	e.preventDefault();

	        	/* Enable Loading Button*/
				var l = Ladda.create(this);
	 			l.start();	  
	 			$(".btn").attr("disabled", true);
	 			/*end loading buttons*/

	        	var url = $(this).data('url');
	        	var postTitle = titleEditor.serialize();
	        	var postContent = editor.serialize();
	        	var amount = AmountEditor.serialize();

	        

	        	axios.post(url, {
				    title: postTitle['post-title']['value']	,
				    amount : amount['post-amount']['value'],
				    content: postContent['element-0']['value']
				})

				.then(function (response) {


					if (response.data.success === false)
					{
						if (response.data.error == 'validation_error')
						{

							var errors = response.data.message;

							$.each( errors, function( key, value ) {
								$(".error").find("ul").append('<li>'+value+'</li>');
								$('.error').fadeTo(2000, 500);

							});


						}else {

							$('.error').find("ul").append(response.data.message);
	                		$('.error').fadeTo(2000, 500).slideUp(500, function(){
	               				$(".error").slideUp(500);
	                		});   

						}
						
					}else {
						location.reload();
					}

					l.stop();
					$(".btn").attr("disabled", false);

				}).catch(function (error) {
				    l.stop();
				});
     		});

     		$('body').on('click', '#addNewChapter' ,function (e) {
	        	e.preventDefault();

	        	/* Enable Loading Button*/
				var l = Ladda.create(this);
	 			l.start();	  
	 			$(".btn").attr("disabled", true);
	 			/*end loading buttons*/

	        	var url = $(this).data('url');
	        	var postTitle = titleEditor.serialize();
	        	var postContent = editor.serialize();

	        	axios.post(url, {
				    title: postTitle['post-title']['value']	,
				    content: postContent['element-0']['value']
				})

				.then(function (response) {

					if (response.data.success === false)
					{
						$('.error').append(response.data.message);
                		$('.error').fadeTo(2000, 500).slideUp(500, function(){
               				$(".error").slideUp(500);
                		});   
					}else {
						window.location.href="{{ route('chapter.create', $book->id)}}";
					}

					l.stop();
					$(".btn").attr("disabled", false);

				}).catch(function (error) {
				    l.stop();
				});
     		});

            
        });

	</script>

@endsection

@section('style')
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css">
	
	<link href="{{ asset('medium/css/medium-editor.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('medium/css/default.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('medium/css/medium-editor-insert-plugin.min.css') }}" rel="stylesheet" type="text/css" />
@endsection