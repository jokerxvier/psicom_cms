@extends('layouts.app')

@section('content')
	<div class="container-fluid book-page">	
    

		<div class="page-content">

      @component('components.errorMessage')
        This is the alert message here.
      @endcomponent
			<div class="row search-page search-content-1">
				<div class="col">

        <div class="search-bar bordered">
              <div class="row">
                  <div class="col-md-12">
                    {!! Form::open(['route' => 'books', 'method' => 'GET',  'class' => 'formControl formDisableOnSubmit', 'id' => 'createBooksForm']) !!}
                        <div class="input-group">
                          
                            {{ Form::input('text', 's', old('s'), ['class' => 'form-control typeahead', 'placeholder' => 'Search for book title', 'id' => 'searchTxt', 'data-url' => route('book.search')]) }}
                          
                            <span class="input-group-btn">
                                <button class="btn blue uppercase bold" type="submit" style="border-radius: 0 !important;">Search</button>
                            </span>
                        </div>

                      {!! Form::close() !!}
                  </div>
                  
              </div>
          </div>

					<div class="search-container bordered clearfix">

            @if ($books->count() > 0)
  						<ul>
  							@foreach($books as $book)
                    <li class="search-item clearfix">
                    		<div class="row">
                    			<div class="col-md-2" style="text-align: center;">
                    				<a href="{{URL::route('chapter', $book->id)}}">
          
                              @component('components.imagesPlaceholder', ['src' => $book->cover, 'width' => '100', 'height' => '156'])
                              @endcomponent

                        		</a>
                    			</div><!--end col-->
                    			<div class="col-md-8 margin-bottom-10">
                    				<div class="search-content">
                    					<h2 class="search-title">

                                @if ($book->type == 'pdf')
                                  <a href="{{URL::route('book.pdf', $book->id)}}">{{ucfirst($book->title)}}</a>
                                @else
                                  <a href="{{URL::route('chapter', $book->id)}}">{{ucfirst($book->title)}}</a>
                                @endif 

                
                                
                            	</h2>

  	                          <p class="search-desc"> 
  	                          	{{str_limit($book->description, $limit = 100, $end = '...')}}
  	                          </p>
  	                          <p></p>
  	                          <p class="search-desc"> 


  	                          	Status : 
                                @if ($book->visibility == 'published')
                                  <label class="label label-primary label-sm"> {{ucfirst($book->visibility)}}</label>

                                @endif

                                @if ($book->visibility == 'unpublished')
                                   <label class="label label-danger label-sm"> {{ucfirst($book->visibility)}}</label>
                                @endif

                           
  	                          </p>
                    					
                    				</div><!--end search-content-->
                    				
                    			</div>
                    			<div class="col-md-2">
                    				
  	                          <a href="{{URL::route('book.edit', $book->id)}}" class="btn btn-default btn-block margin-bottom-10"> Edit </a>
                              @if($book->visibility == 'unpublished')
                                <a href="{{URL::route('book.update.status', ['book_id' => $book->id])}}" class="btn btn-default btn-block margin-bottom-10 blue"> Publish </a>
                              @endif

                              @if($book->visibility == 'published')
                                <a href="{{URL::route('book.update.status', ['book_id' => $book->id])}}" class="btn btn-default btn-block margin-bottom-10 red"> Unpublish </a>
                              @endif
                              

                              {!! Form::open(['route' => ['book.delete', $book->id], 'method' => 'DELETE', 'onsubmit' => 'return confirm("are you sure ?")']) !!}
  	                             {{ method_field('DELETE') }}
  	                             <button type="submit" class="btn btn-default  btn-block margin-bottom-10">Delete </button>
                              {!! Form::close() !!}

                        
                        	
                    			</div>
                    		</div><!--end row-->
   
                    </li>
                @endforeach
                                                     
              </ul>

              <div class="search-pagination">{!! $books->appends(['s' => app('request')->input('s')])->render() !!}</div>
            @endif


            @if ($books->count() <= 0 && !app('request')->input('s') )
              <div class="col-md-12 page-500">
              
                  <div class=" details">
                      <h3>You haven't written any stories yet.</h3>
                    
                      <p>
                          <a href="{{URL::route('book.create')}}" class="btn red btn-outline"> Create  a  Book</a>
                          <br> 
                      </p>
                  </div>
              </div>
            @endif

            @if ($books->count() <= 0 && app('request')->input('s') )
              <div class="col-md-12 page-500">
              
                  <div class=" details">
                      <h3>Result not Found!</h3>

                      <a href="{{URL::route('books')}}" class="btn red btn-outline"> Go Back</a>
                      <p></p>
                  </div>
              </div>
            @endif
					</div><!--end search-container-->

				</div><!--end col-->

			</div><!--end row-->

		</div><!--end page-content-->
	</div><!--end container-->

@endsection


@section('style')
	<link href="{{ asset('themes/pages/css/search.min.css') }}" rel="stylesheet" type="text/css" />

@stop

@section('script')
  {{ Html::script('js/books.js') }}  
@stop

