@extends('layouts.app')

@section('content')

	<div class="container-fluid book-page">	

		<div class="page-content">

        <div style="padding : 10px">
            @component('components.infoMessage')
                @slot('message')
                    Author cannot delete the chapters if there are already users who bought it.
                @endslot
            @endcomponent
        </div>



		@component('components.successMessage') @endcomponent
		@component('components.errorMessage')
            This is the alert message here.
        @endcomponent
			<div class="page-content-row">
				@component('book.bookSidebar', ['book_id' => $book->id, 'book' => $book]) @endcomponent

				<div class="page-content-col">
					<div class="portlet light portlet-fit bordered">
					
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-dark"></i>
                                <span class="caption-subject font-dark sbold uppercase">Table of Contents</span>
                            </div>

                            <div class="actions">
                                
                            	<a href="{{ route('chapter.create', $book->id)}}"  class="btn btn-circle  btn-warning">
                                   Create new chapter
                                </a>
			                   
                            </div><!--end actions-->
                        </div><!--end portlet-title-->

                        <div class="portlet-body">
                            @if ($chapters->count() > 0)
                            	<div class="table-scrollable-borderless">
                            		<table class="table table-hover table-light">
                            			<tbody  id="sortable">
                                            @foreach ($chapters as $chapter)
                                				<tr id="chapter-{{$chapter->id}}">
                                                    <td class="handle">
                                                       <span class="item">
                                                        <span aria-hidden="true" class="icon-list"></span> 
                                                       </span>
                                                    </td>
                         
                                                    <td> 
                                                    	<a href="{{URL::route('chapter.edit', [$book->id, $chapter->id])}}" style="color: #444;"> {{$chapter->title}} </a><br />
                                                          
                                                    	<span style="color: #717171; font-size: 12px;"> {{ucfirst($chapter->visibility)}} | {{ucfirst($chapter->created_at->format('M d Y'))}} | {{ ' Points : ' . $chapter->points }} | People who bought : {{$chapter->inventory()->count()}}</span>
                                                    </td>

                                         
                                                    <td width="10%">
                                                        <div class="actions">
                                                            <div class="btn-group">
                                                                <a class="btn dark btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> Actions
                                                                    <i class="fa fa-angle-down"></i>
                                                                </a>
                                                                <ul class="dropdown-menu pull-right">
                                                                    <li>
                                                                        <a href="{{URL::route('chapter.edit', [$book->id, $chapter->id])}}"> Edit </a>
                                                                    </li>
                                                                    <li class="divider"> </li>
                                                                    <li>
                                                                        {!! Form::open(['url' =>  route('chapter.delete', [$book->id, $chapter->id]), 'onsubmit' => 'return confirm("Are you sure ?")' ]) !!}
                                                                                <button type="submit" class="btn btn-link"> Delete </button>
                                                                        {!! Form::close() !!}
                                                                    </li>
                                                                    
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach

                            			</tbody>
                            		</table>


                            	</div><!--end table-scrollable-->
                            @endif
                            @if ($chapters->count() <= 0)
                              <div class="col-md-12 page-500">
                              
                                  <div class=" details">
                                      <h3>You haven't written any stories yet.</h3>
                                    
                                      
                                  </div>
                              </div>

                              <div class="clearfix"></div>
                            @endif

                        </div><!--end portlet-body-->


                            
                

					</div><!--end portlet-->
				</div><!--end page content-col-->
			</div><!--end page-content-row-->

		
		</div><!--end page-content-->

	</div><!--end container-fluid-->

@endsection

@section('script')
	{{ Html::script('themes/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }} 


    <script type="text/javascript">
        $( function() {
            var url = "{{URL::route('chapter.sortable', $book->id)}}"
            $( "#sortable" ).sortable({
                handle : '.handle', 
                update : function () { 
                    var order = $('#sortable').sortable('serialize'); 
                    axios.post(url, order)
                    .then(function (response) {

                        if(!response.data.success) {
                            console.log('Whoops, something went wrong :/');
                        }
            
                    })
                    .catch(function (error) {
                        if (error)
                        {
                            console.log('Whoops, something went wrong :/');
                        }
                    });
                }
            });
 
        });
    </script>

@stop