<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".page-sidebar">
    <span class="sr-only">Toggle navigation</span>
    <span class="toggle-icon">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </span>
</button>
                
<div class="page-sidebar">
    <nav class="navbar" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <!-- Collect the nav links, forms, and other content for toggling -->
        <ul class="nav navbar-nav margin-bottom-35">
            @if($book->type == 'book')
                <li class="{{set_active('books/chapters/*') }}" >
                    <a href="{{URL::route('chapter', $book_id)}}">
                        <i class="icon-home"></i> Chapters </a>
                </li>
            @endif

            @if($book->type == 'pdf')
                <li class="{{set_active('books/pdf/*') }}" >
                    <a href="{{URL::route('book.pdf', $book_id)}}">
                        <i class="icon-home"></i> PDF </a>
                </li>
            @endif


            <li class="{{set_active('books/details/*') }}">
                <a href="{{URL::route('book.edit', $book_id)}}">
                    <i class="icon-note "></i> Books Details </a>
            </li>
            
        </ul>
     
    </nav>
</div>