{!! Form::open(['route' => 'remittance.save', 'method' => 'POST', 'files' => true, 'class' => 'formControl formDisableOnSubmit']) !!}
    <div class="form-group {{ $errors->has('bank') ? ' has-error' : '' }} form-md-line-input">
        {{ Form::select('bank', $banks, isset($user->bank_info->bank_id) ? $user->bank_info->bank_id : null  , ['class' => 'form-control', 'placeholder' => 'Select Remittance']) }}

        <label for="form_control_1">Remittance Name</label>

        @if ($errors->has('bank'))
            <span class="help-block">
                <strong>{{ $errors->first('bank') }}</strong>
            </span>
        @endif
       
    </div><!--end form-group-->



    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} form-md-line-input">
        {{ Form::input('name', 'name', isset($user->bank_info->name) ? $user->bank_info->name : null, ['class' => 'form-control']) }}

        <label for="form_control_1">Recipient Name</label>

        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
       
    </div><!--end form-group-->


    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }} form-md-line-input">
        {{ Form::input('address', 'address', isset($user->bank_info->address) ? $user->bank_info->address : '' , ['class' => 'form-control']) }}

        <label for="form_control_1">Address</label>

        @if ($errors->has('address'))
            <span class="help-block">
                <strong>{{ $errors->first('address') }}</strong>
            </span>
        @endif
       
    </div><!--end form-group-->


    <div class="form-group {{ $errors->has('contact_number') ? ' has-error' : '' }} form-md-line-input">
        {{ Form::input('contact_number', 'contact_number', isset($user->bank_info->contact_number) ? $user->bank_info->contact_number : '' , ['class' => 'form-control']) }}

        <label for="form_control_1">Contact Number</label>

        @if ($errors->has('contact_number'))
            <span class="help-block">
                <strong>{{ $errors->first('contact_number') }}</strong>
            </span>
        @endif
       
    </div><!--end form-group-->

    <div class="row">

        <div class="col-md-6 col-md-offset-4">
            <div class="btn-group">
                <a href="{{URL::route('profile')}}#bankInfo" class="btn red btn-lg margin-right-10">Cancel</a>
                <button type="submit" class="btn green btn-lg ">Save</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
