@extends('layouts.app')

@section('content')
	<div class="container-fluid book-page">	

		<div class="page-content">
		
	
	    	<div class="row">

		    	<div class="col-md-8 col-md-offset-2">

		    		@component('components.successMessage') @endcomponent
					@component('components.errorMessage')
			            This is the alert message here.
			        @endcomponent

		    		<div class="portlet light bordered">

		    			<div class="portlet-body">
							@component('bank.remittanceComponent', ['banks' => $banks, 'user' => $user]) @endcomponent
						</div>
					</div>
				</div>
			</div>
	




		</div><!--end page-content-->

	</div><!--end container-fluid-->
@endsection
