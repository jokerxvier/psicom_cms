{!! Form::open(['route' => 'bank.save', 'method' => 'POST', 'files' => true, 'class' => 'formControl formDisableOnSubmit']) !!}
    <div class="form-group {{ $errors->has('bank') ? ' has-error' : '' }} form-md-line-input">
        {{ Form::select('bank', $banks, isset($user->bank_info->bank_id) ? $user->bank_info->bank_id : null  , ['class' => 'form-control', 'placeholder' => 'Select Bank']) }}

        <label for="form_control_1">Account Name</label>

        @if ($errors->has('bank'))
            <span class="help-block">
                <strong>{{ $errors->first('bank') }}</strong>
            </span>
        @endif
       
    </div><!--end form-group-->



    <div class="form-group {{ $errors->has('account_name') ? ' has-error' : '' }} form-md-line-input">
        {{ Form::input('account_name', 'account_name', isset($user->bank_info->name) ? $user->bank_info->name : null, ['class' => 'form-control']) }}

        <label for="form_control_1">Account Name</label>

        @if ($errors->has('account_name'))
            <span class="help-block">
                <strong>{{ $errors->first('account_name') }}</strong>
            </span>
        @endif
       
    </div><!--end form-group-->


    <div class="form-group {{ $errors->has('account_number') ? ' has-error' : '' }} form-md-line-input">
        {{ Form::input('account_number', 'account_number', isset($user->bank_info->account_number) ? $user->bank_info->account_number : '' , ['class' => 'form-control']) }}

        <label for="form_control_1">Account Number</label>

        @if ($errors->has('account_number'))
            <span class="help-block">
                <strong>{{ $errors->first('account_number') }}</strong>
            </span>
        @endif
       
    </div><!--end form-group-->


    <div class="row">

        <div class="col-md-6 col-md-offset-4">
            <div class="btn-group">
                <a href="{{URL::route('profile')}}#bankInfo" class="btn red btn-lg margin-right-10">Cancel</a>
                <button type="submit" class="btn green btn-lg ">Save</button>
            </div>
        </div>
    </div>
{!! Form::close() !!}
