@extends('layouts.error')

@section('content')
	 <div class="row">
            <div class="col-md-12 page-404">
                <div class="number font-red"> 503 </div>
                <div class="details">
                    <h3>Oops! {{ $exception->getMessage() }}</h3>
                    <p> 
                        <br/>
                        <a href="/"> Return home </a>
                    </p>
                   
                </div>
            </div>
        </div>

@endsection


@section('style')

	<link href="{{ asset('themes/pages/css/error.min.css') }}" rel="stylesheet" type="text/css" />

@stop