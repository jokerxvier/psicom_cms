<header class="page-header">
	<nav class="navbar mega-menu" role="navigation">
        <div class="container-fluid">
            <div class="clearfix navbar-fixed-top">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="toggle-icon">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </span>
                </button>
              
                <a id="index" class="page-logo" href="{{route('home')}}">
                    <img src="{{asset('images/psicom_author_logo.png')}}" alt="Logo"> </a>
                <!-- END LOGO -->
               
                <!-- END SEARCH -->
                <!-- BEGIN TOPBAR ACTIONS -->
                <div class="topbar-actions">
                    <!-- BEGIN GROUP NOTIFICATION -->
                    
                    <!-- END GROUP NOTIFICATION -->
                    <!-- BEGIN GROUP INFORMATION -->
                    <div class="btn-group-red btn-group">
                        <a href="{{URL::route('book.create')}}" class="btn btn-medium btn-circle" style="width: 113px; height: auto !important; background: #f1bf2c">
                            Create Books
                        </a>
                        
                    </div>
                    <!-- END GROUP INFORMATION -->

                    @if(Auth::check())
                        <!-- BEGIN USER PROFILE -->
                        <div class="btn-group-img btn-group">
                            <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <span>Hi, {{Auth::user()->author_name}}</span>
                                <img src="{{ Auth::user()->profile_pic  }}" alt=""> </button>

                            <ul class="dropdown-menu-v2" role="menu">
                                <li>
                                    <a href="{{URL::route('profile')}}">
                                        <i class="icon-user"></i> My Profile
                                        <!--<span class="badge badge-danger">1</span>-->
                                    </a>
                                </li>
                               
                                <li>
                                    <a href="{{URL::route('logout')}}">
                                        <i class="icon-key"></i> Log Out </a>
                                </li>
                            </ul>

                            
                        </div><!--end btn-group-->
                        <!-- END USER PROFILE -->
                    @endif
                   
                </div>
                <!-- END TOPBAR ACTIONS -->
            </div>

            @if (!Request::is('invite'))
                <!-- BEGIN HEADER MENU -->
                <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown active open selected ">
                            <a href="{{URL::route('books')}}" class="text-uppercase">
                                <i class="icon-book-open"></i> My Stories </a>
                            
                        </li>
                        <!--<li class="dropdown dropdown-fw dropdown-fw-disabled  ">
                            <a href="javascript:;" class="text-uppercase">
                                <i class="fa fa-image"></i> My Artworks </a>
                        </li>-->
                        
                    </ul>
                </div>
                <!-- END HEADER MENU -->
            @endif
        </div>
        <!--/container-->
    </nav>
	

</header>