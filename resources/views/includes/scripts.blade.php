<script type="text/javascript" src="{{asset('themes/global/plugins/jquery.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/jquery-ui.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/jquery.ui.touch-punch.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/axios.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('themes/global/plugins/bootstrap/js/bootstrap.min.js')}}"></script> 

<script type="text/javascript" src="{{asset('themes/global/plugins/file-upload/vendor/jquery.ui.widget.js')}}"></script> 
<script type="text/javascript" src="{{asset('js/load-image.all.min.js') }}"></script> 

<script type="text/javascript" src="{{asset('themes/global/plugins/moment.min.js')}} "></script> 

<script type="text/javascript" src="{{asset('themes/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('themes/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('themes/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('themes/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('themes/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('themes/global/plugins/bootstrap-typehead/bootstrap3-typeahead.min.js')}}"></script> 

<script type="text/javascript" src="{{asset('themes/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('themes/global/plugins/ladda/spin.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('themes/global/plugins/ladda/ladda.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('themes/global/plugins/jquery_lazyload/jquery.lazy.min.js')}}"></script> 
<script type="text/javascript" src="{{asset('themes/global/plugins/jquery_lazyload/jquery.lazy.plugins.min.js')}}"></script> 

<script type="text/javascript" src="{{asset('themes/global/scripts/app.min.js')}}"></script> 

<script type="text/javascript" src="{{asset('themes/global/plugins/bootstrap-datetimepicker/js/components-date-time-pickers.min.js')}}"></script> 

<script type="text/javascript" src="{{asset('js/psicom.js')}}"></script>  




 @yield('script')



 <script type="text/javascript">
 		$("img.lazy").Lazy();
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
  </script>
