<link href="{{ asset('https://fonts.googleapis.com/css?family=Oswald:400,300,700') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('themes/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('themes/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('themes/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />



<link href="{{ asset('themes/global/plugins/ladda/ladda-themeless.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('themes/global/css/components.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('themes/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('themes/layouts/layout5/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('themes/layouts/layout5/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="{{asset('themes/global/plugins/file-upload/css/jquery.fileupload.css')}}">
<link href="{{ asset('themes/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />

@if ( Request::is('login') || Request::is('register') ||  Request::is('password/*'))
 <link href="{{ asset('themes/pages/css/login.css') }}" rel="stylesheet" type="text/css" />
@endif

 <link href="{{ asset('themes/pages/css/author.css') }}" rel="stylesheet" type="text/css" />
 @yield('style')

 <link href="{{ asset('themes/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />

