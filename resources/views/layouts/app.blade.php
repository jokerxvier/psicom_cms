<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PSICOM</title>

    <link rel="shortcut icon" href="{{{ asset('images/favicon.ico') }}}">

    <!-- Styles -->
    @include('includes.styles')

</head>

@if (Auth::check() && Auth::user()->account_type == 'author')
    <body id="author" class="page-header-fixed page-sidebar-closed-hide-logo">
@elseif (Auth::check() && Auth::user()->account_type == 'admin')
    <body id="admin" class="page-header-fixed page-sidebar-closed-hide-logo">

@else 
    <body class="{{ Request::is('login')  || Request::is('register') ||  Request::is('password/*')   ? 'login' : 'page-header-fixed page-sidebar-closed-hide-logo' }}">
@endif



<body class="{{ Request::is('login') ||  Request::is('invite')   || Request::is('register')  ||  Request::is('password/*')   ? 'login' : 'page-header-fixed page-sidebar-closed-hide-logo' }}">
    <div id="app">
        <div class="wrapper">
            @if(Auth::check())
                @include('includes.header')
            @endif
            @yield('content')
        </div><!--end wrapper-->
        
    </div>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}"></script>--}}

    @include('includes.scripts')

    <script type="text/javascript">
        
        $(function () {
          $('[data-toggle="popover"]').popover({
            container: 'body',
            trigger : 'click'
          })

          $('html').on('click', function(e) {
              if (typeof $(e.target).data('original-title') == 'undefined') {
                $('[data-original-title]').popover('hide');
              }
          });


        });

    </script>

    


</body>
</html>
