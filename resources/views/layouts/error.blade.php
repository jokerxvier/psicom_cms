<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PSICOM</title>
    <link rel="shortcut icon" href="{{{ asset('images/favicon.ico') }}}">

    <!-- Styles -->
    @include('includes.styles')

</head>

<body class=" page-404-full-page">
	<div class="row">
	    <div class="col-md-12 page-404">
	    	@yield('content')
	        
	    </div>
	</div>

</body>
</html>
