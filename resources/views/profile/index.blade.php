@extends('layouts.app')

@section('content')

@if (Auth::check())



		<div class="container-fluid book-page">	
			<div class="page-content">
				<div class="page-content-col">
					<div class="row">
						<div class="col-md-12">
							

							<div class="profile-sidebar">
								<div class="portlet light profile-sidebar-portlet bordered">
									<div class="profile-userpic">
											@component('components.imagesPlaceholder', ['src' => Auth::user()->profile_pic, 'width' => '200', 'height' => '200', 'class' => 'img-responsive'])

                  		@endcomponent
	                   
	                </div><!--end profile-userpic-->
	                <div class="profile-usertitle">
	                    <div class="profile-usertitle-name"> {{ Auth::user()->author_name }}</div>
	                    <div class="profile-usertitle-job"> {{ Auth::user()->account_type }} </div>
	                </div>
								</div><!--end protlet-->
							</div><!--end profile-sidebar-->

							<div class="profile-content">
								<div class="row">
									<div class="col-md-12">
										<div class="portlet light bordered">
											<div class="portlet-title tabbable-line">

													<div class="caption caption-md">
														<span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
			                    </div><!--end caption-->

			                    <ul class="nav nav-tabs">
			                        <li class="active">
			                            <a href="#userinfo" data-toggle="tab">Personal Info</a>
			                        </li>
			                        <li>
			                            <a href="#userAvatar" data-toggle="tab">Change Avatar</a>
			                        </li>
			                        <li>
			                            <a href="#userPassword" data-toggle="tab">Change Password</a>
			                        </li>

			                        <li>
			                            <a href="#bankInfo" data-toggle="tab">Bank Information</a>
			                        </li>
			                        
			                    </ul>
			                  </div><!--end portlet-title-->

			                  <div class="portlet-body">
			                  	<div class="tab-content">
			                  		@component('components.errorMessage') @endcomponent
														@component('components.successMessage') @endcomponent

			                  		@component('profile.userinfo_comp') @endcomponent
			                  
			                  		@component('profile.avatar') @endcomponent
			              
			                  		@component('profile.change_password') @endcomponent

			                  		@component('profile.bank_info') @endcomponent
			     								</div><!--end tab-content-->

			                  </div><!--end portlet-body-->

										</div><!--end portlet-->
									</div><!--end col-md-12-->

								</div><!--end row-->
	                        	
	            </div><!--end profile-content-->
						</div><!--end col-md-12-->
					</div><!--end row-->
				</div><!--end page-content-col-->

			</div><!--end page-content-->


		</div><!--end container-fluid-->
@endif

@endsection

@section('style')
	<link href="{{ asset('themes/pages/css/profile.min.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('script')
	{{ Html::script('themes/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}       
@stop

