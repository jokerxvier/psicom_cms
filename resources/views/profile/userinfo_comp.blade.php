<div class="tab-pane active" id="userinfo">
 	{!! Form::open(['url' =>  route('profile.update'), 'class' => 'formDisableOnSubmit' ]) !!}

      
        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} form-md-line-input">
            {{ Form::input('email', 'email', Auth::user()->email, ['class' => 'form-control', 'disabled' => 'disabled']) }}

            <label for="form_control_1">Email</label>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
           
        </div><!--end form-group-->

        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} form-md-line-input">
            {{ Form::input('username', 'username', Auth::user()->username, ['class' => 'form-control', 'disabled' => 'disabled']) }}

            <label for="form_control_1">Username</label>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
           
        </div><!--end form-group-->

        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} form-md-line-input">
            {{ Form::input('name', 'name', Auth::user()->name, ['class' => 'form-control']) }}

            <label for="form_control_1">Name</label>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
           
        </div><!--end form-group-->

        <div class="form-group {{ $errors->has('birthday') ? ' has-error' : '' }} form-md-line-input">
            {{ Form::input('birthday', 'birthday', Auth::user()->birthday, ['class' => 'form-control date-picker' , 'data-date-format' => "yyyy/mm/dd"]) }}

            <label for="form_control_1">Birthday</label>

            @if ($errors->has('birthday'))
                <span class="help-block">
                    <strong>{{ $errors->first('birthday') }}</strong>
                </span>
            @endif
           
        </div><!--end form-group-->

        <div class="form-group {{ $errors->has('location') ? ' has-error' : '' }} form-md-line-input">
            {{ Form::input('location', 'location', Auth::user()->location, ['class' => 'form-control']) }}

            <label for="form_control_1">Location</label>

            @if ($errors->has('location'))
                <span class="help-block">
                    <strong>{{ $errors->first('location') }}</strong>
                </span>
            @endif
           
        </div><!--end form-group-->

        <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }} form-md-radios">
            <label for="form_control_1">Gender</label>


            <div class="md-radio-inline">
                <div class="md-radio">
                    <input type="radio" id="radio6" name="gender" class="md-radiobtn" value="M" {{Auth::user()->gender == 'M' ? 'checked' : ''}}>
                    <label for="radio6">
                        <span class="inc"></span>
                        <span class="check"></span>
                        <span class="box"></span> Male</label>
                </div>
                <div class="md-radio">
                    <input type="radio" id="radio7" name="gender" class="md-radiobtn" value="F" {{Auth::user()->gender == 'F' ? 'checked' : ''}}>
                    <label for="radio7">
                        <span class="inc"></span>
                        <span class="check"></span>
                        <span class="box"></span>Female</label>
                </div>
                
            </div>

            @if ($errors->has('gender'))
                <span class="help-block">
                    <strong>{{ $errors->first('gender') }}</strong>
                </span>
            @endif

        </div><!--end form-group-->

        <div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }} form-md-checkboxes">
            <label for="form_control_1">Set Author Name</label>

            <div class="md-radio-inline">
                <div class="md-radio">
                    <input type="radio" id="radio8" name="set_author_name" class="md-radiobtn" value="username" {{Auth::user()->set_author_name == 'username' ? 'checked' : ''}}>
                    <label for="radio8">
                        <span class="inc"></span>
                        <span class="check"></span>
                        <span class="box"></span> Show my username as author name</label>
                </div>
                <div class="md-radio">
                    <input type="radio" id="radio9" name="set_author_name" class="md-radiobtn" value="name" {{Auth::user()->set_author_name == 'name' ? 'checked' : ''}}>
                    <label for="radio9">
                        <span class="inc"></span>
                        <span class="check"></span>
                        <span class="box"></span>Show my real name as author name</label>
                </div>
                
            </div>

           

        </div>



        <div class="margiv-top-10">
            <button type="submit" class="btn green"> Save Changes </button>
           
        </div>


    {!! Form::close() !!}
</div><!--tab-pane-->