<div class="tab-pane" id="userPassword">
	{!! Form::open(['url' =>  route('profile.change.pass'), 'class' => 'formDisableOnSubmit' ]) !!}

      
        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} form-md-line-input">
            {{ Form::input('password', 'old_password', null, ['class' => 'form-control']) }}

            <label for="form_control_1">Current Password</label>

            @if ($errors->has('old_password'))
                <span class="help-block">
                    <strong>{{ $errors->first('old_password') }}</strong>
                </span>
            @endif
           
        </div><!--end form-group-->

        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} form-md-line-input">
            {{ Form::input('password', 'password', null, ['class' => 'form-control']) }}

            <label for="form_control_1">New Password</label>

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
           
        </div><!--end form-group-->

        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }} form-md-line-input">
            {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control']) }}

            <label for="form_control_1">Re-type New Password</label>

            @if ($errors->has('password_confirmation'))
                <span class="help-block">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
           
        </div><!--end form-group-->

       

        <div class="margiv-top-10">
            <button class="btn green"> Save Changes </button>
           
        </div>


    {!! Form::close() !!}
</div><!--end userAvatar-->