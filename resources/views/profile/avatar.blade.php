<div class="tab-pane" id="userAvatar">
   
    @component('components.infoMessage')
        @slot('message')
            Must be in PNG,  or JPG format, smaller than 2MB. Recommended cover dimensions: 200x200 pixels
        @endslot
    @endcomponent
        

	{!! Form::open(['url' =>  route('profile.avatar.upload'), 'class' => 'formDisableOnSubmit', 'files' => true ]) !!}
        <div class="form-group">
            <div class="fileinput fileinput-new" data-provides="fileinput">
                <div class="fileinput-new thumbnail" style="width: 200px; height: 200px;">
                  @component('components.imagesPlaceholder', ['src' => 'http://www.placehold.it/200x200/EFEFEF/AAAAAA&amp;text=no+image', 'width' => '200', 'height' => '200'])

                  @endcomponent


                </div>
                <div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 200px;"> </div>
                <div>
                        <span class="btn default btn-file">
                            <span class="fileinput-new"> Select image </span>
                            <span class="fileinput-exists"> Change </span>
                            <input type="file" name="avatar"> 
                        </span>
                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                </div>
            </div>
            
        </div>
        <div class="margin-top-10">
            <button  type="submit" class="btn green"> Submit </button>
        </div>
    {!! Form::close() !!}
</div><!--end userAvatar-->