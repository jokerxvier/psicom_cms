<div class="tab-pane" id="bankInfo">

    @component('components.infoMessage')
        @slot('message')
           Select one payout method below. You can update or change your payout method at any time but you can only use one from the available options.
        @endslot
    @endcomponent


 
      	
      	@if(Auth::user()->bank_info()->count() <= 0)
        	<a href="{{route('remittance')}}" class="btn green-sharp btn-outline  btn-block sbold uppercase"> Set up Remittance <br /> Information </a>
 
        	<a href="{{route('bank')}}" class="btn green-sharp btn-outline  btn-block sbold uppercase">Set up Bank <br /> Information</a>
        @endif


        <div class="row">
          @if (Auth::user()->bank_info()->count() > 0)
          	<div class="col-md-12">
  	        	<div class="m-heading-1 border-green m-bordered">
  	                 @if (Auth::user()->bank_info->bank->type == 'bank')
  	                 	<h3>Bank : {{ Auth::user()->bank_info->bank->name }}</h3>
  	                 	<p><strong>Account Number</strong> : {{Auth::user()->bank_info->account_number}}</p>
  	                 	<p><strong>Account Name</strong> : {{Auth::user()->bank_info->name}}</p>

  	                 	<br />
  	                 	<a class="btn green btn-outline btn-xs" href="{{route('bank')}}">Edit</a>
  	                 	<a class="btn red btn-outline btn-xs" href="{{route('bank.delete', Auth::user()->bank_info->id)}}">Delete</a>

  	                 @endif


                     @if (Auth::user()->bank_info->bank->type == 'remittance')
                      <h3>Remittance : {{ Auth::user()->bank_info->bank->name }}</h3>
                      <p><strong>Name</strong> : {{Auth::user()->bank_info->name}}</p>
                      <p><strong>Address</strong> : {{Auth::user()->bank_info->address}}</p>
                      <p><strong>Contact Number</strong> : {{Auth::user()->bank_info->contact_number}}</p>

                      <br />
                      <a class="btn green btn-outline btn-xs" href="{{route('remittance')}}">Edit</a>
                      <a class="btn red btn-outline btn-xs" href="{{route('remittance.delete', Auth::user()->bank_info->id)}}">Delete</a>

                     @endif
  	            </div>                     
            </div>

         	@endif
      </div>



</div><!--end userAvatar-->

