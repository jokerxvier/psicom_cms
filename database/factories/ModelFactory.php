<?php
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'username' => $faker->unique()->userName,
        'password' => \Hash::make('test'),
        'profile_pic' => 'https://s3.amazonaws.com/psicom/default.png',
        'birthday' => $faker->date,
        'about' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        'gender' => 'M',
        'location' => $faker->city . ', ' . $faker->country,
        'remember_token' => str_random(10),
    ];

    
});

$factory->define(App\Models\Prices::class, function (Faker\Generator $faker) {
    return [
        'amount' => 1,
    ];
});



$factory->define(App\Models\Categories::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word
    ];
});


$factory->define(App\Models\Membership::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'type' => 'free'
    ];
});


$factory->define(App\Models\Books::class, function (Faker\Generator $faker) {

    $users = App\User::all()->pluck('id')->toArray();
    $membership = App\Models\Membership::where('type', '=', 'upgrade')
                    ->orWhere('type', '=', 'free')
                    ->pluck('id')->toArray();

    return [
        'title' => $faker->text($maxNbChars = 20),
        'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'ratings' => 'general',
        'visibility' => 'published',
        'points' => rand(10, 1000),
        'type' => 'book',
        'release_date' => Carbon::now(),
        'membership_type' =>  $faker->randomElement($membership),
        'author_id' => $faker->randomElement($users)
    ];
});

$factory->define(App\Models\Featured::class, function (Faker\Generator $faker) {

    $books = App\Models\Books::all()->pluck('id')->toArray();
    return [
        'book_id' => $faker->randomElement($books),
        
    ];
});


$factory->define(App\Models\Likes::class, function (Faker\Generator $faker) {

    $books = App\Models\Books::all()->pluck('id')->toArray();
    $chapters = App\Models\Chapters::all()->pluck('id')->toArray();
    $user = App\User::all()->pluck('id')->toArray();

    return [
        'book_id' => $faker->randomElement($books),
        'chapter_id' => $faker->randomElement($books),
        'user_id' => $faker->randomElement($user),
        
    ];
});

$factory->define(App\Models\Promos::class, function (Faker\Generator $faker) {

    $membership = App\Models\Membership::where('type', '=', 'upgrade')
                    ->orWhere('type', '=', 'free')
                    ->pluck('id')->toArray();

    return [
        'title' => $faker->text($maxNbChars = 20),
        'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'points' => rand(10, 100),
        'image_path' => 'http://via.placeholder.com/800x400',
        'membership_id' =>  $faker->randomElement($membership),
        'status' => 'disabled'
    ];
});

$factory->define(App\Models\Avatars::class, function (Faker\Generator $faker) {
     $membership = App\Models\Membership::where('type', '=', 'upgrade')
                    ->orWhere('type', '=', 'free')
                    ->pluck('id')->toArray();

    return [
        'title' => $faker->text($maxNbChars = 20),
        'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'points' => rand(10, 100),
        'image_path' => 'http://via.placeholder.com/400x400',
        'status' => 'active',
        'membership_id' =>  $faker->randomElement($membership),
    ];
});


$factory->define(App\Models\Adverts::class, function (Faker\Generator $faker) {
     
    return [
        'title' => $faker->text($maxNbChars = 20),
        'image_path' => 'http://via.placeholder.com/750x300',
        'redirect_url' => 'http://google.com',
        'status' => 'active',
    ];
});

$factory->define(App\Models\Cards::class, function (Faker\Generator $faker) {
     
    return [
        'title' => $faker->text($maxNbChars = 20),
        'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'sku' => 'sku',
        'price' => rand(10, 100),
        'points' => rand(10, 100),
        'type' => 'topup'
    ];
});


$factory->define(App\Models\Comments::class, function (Faker\Generator $faker) {

    $user = App\User::where('account_type', '=', 'default')->pluck('id')->toArray();
    $books = App\Models\Books::where('visibility', '=', 'published')->pluck('id')->toArray();
     
    return [
        'title' => $faker->text($maxNbChars = 20),
        'message' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'book_id' => $faker->randomElement($books),
        'user_id' => $faker->randomElement($user),
        
    ];
});

$factory->define(App\Models\Pages::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->text($maxNbChars = 20),
        'content' => '<p>Lorem ipsum dolor sit amet, id habeo iudicabit definiebas pro, suas rationibus eam et, sumo accusam recteque vim id. No usu putant delectus. Mel purto viris posidonium ad. Cum errem definiebas ut, ne autem aliquid pertinacia quo. Mel ocurreret ullamcorper ex, qui oratio sensibus similique ut. Qui id erant zril, qui ne legere tacimates pertinacia. Accusata eleifend consetetur mea at.</p><p>Nec legimus legendos disputando id, solum habemus commune vel ne. Homero admodum consetetur at nam, ne dicta putent fabulas vis. Purto aeterno philosophia no vis, summo nulla latine te qui. Vim affert detraxit dissentias te, clita everti ad usu.</p><p>Et tota harum copiosae sea, ne appareat forensibus nec, vis nihil delectus fabellas cu. Duo appareat consequat at. Eius evertitur at qui, id pro esse ferri accusam, cu natum eripuit vulputate pro. Cum ei dictas regione. Vix labore malorum deterruisset ad, euismod commune ei nec, vis solet laoreet te.</p><p>Tota nobis nemore quo no, ex nobis bonorum iracundia mea. Brute liber possit at vim, duo nibh numquam mentitum ei, summo gloriatur te mea. Congue apeirian recusabo est eu, id sint detracto iudicabit quo, etiam accumsan ne has. Unum postea suscipiantur his at, usu id sanctus suscipiantur. No vim partem verear explicari, qui liber nemore tacimates ad. <br /><br />In eam malorum splendide. Ipsum indoctum euripidis ea per, facete corrumpit at sea. Ad quo commodo euripidis. Te mel novum facilis periculis, sed dolor tation omittantur an. Aeterno oblique hendrerit cu quo.</p><p>Augue graece dolorum pri ne, eruditi elaboraret temporibus ei ius, mea et dolore legimus adipisci. Cum ex enim numquam. Justo blandit te pri, ei nam libris concludaturque. In erat choro vim, sit dicta exerci te. Ex duo essent aliquip volumus, quis putant honestatis mel no. Probatus mnesarchum sed ne, in graece putent recusabo sed. Te sit libris pericula, ea populo graeci mea, nec no eruditi temporibus.</p><p>Primis tritani accusamus cum ad, natum phaedrum corrumpit eu vim. Sed id debitis periculis, argumentum reprehendunt eu sea, vix alterum blandit hendrerit no. Ex eius numquam mel. Habeo nulla has ut, ne etiam quaestio pro. Aperiam accusamus adolescens te vix, ius id saepe propriae, qui dolores phaedrum repudiandae te. Ad propriae similique his, sed at officiis consequat omittantur, eruditi aliquando et duo.</p><p>In offendit recusabo tincidunt vix. Iudico quodsi et vim, vel detracto legendos deserunt ad. Te qui probo signiferumque, et vis vocent eleifend partiendo, tation aliquam assueverit ius ex. At alii quas his.</p><p>Maiorum noluisse vituperata sea ut, at singulis abhorreant sit. Quo malis homero blandit id. Usu amet petentium delicatissimi ad, eam te mutat aperiri, perfecto quaerendum an duo. Eam impetus aliquid ne, quot vidisse ex est, eos latine impetus nostrum ut. Eos duis wisi mediocrem in.</p><p>In mel rebum posidonium. Utroque consequuntur pri in, stet cibo at sit. Eam clita putent feugiat ad, vis te modo senserit maluisset. Zril ubique expetenda ex eum.</p>',
            
            'page_name' => 'default'
      
        
    ];

});


$factory->define(App\Models\Banks::class, function (Faker\Generator $faker) {

     return [
        'name' => $faker->text($maxNbChars = 20),
    ];

});



$factory->define(App\Models\CardClassification::class, function (Faker\Generator $faker) {

     return [
        'name' => $faker->text($maxNbChars = 20),
    ];

});



