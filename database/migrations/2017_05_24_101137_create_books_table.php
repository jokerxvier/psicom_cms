<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();

        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('description');
            $table->enum('ratings', ['mature', 'general'])->default('general');
            $table->enum('visibility', ['published', 'unpublished', 'deleted'])->default('unpublished');
            $table->integer('membership_type')->unasigned()->index();
            $table->integer('author_id')->unasigned()->index();
            $table->date('release_date')->nullable();
            $table->enum('type', ['book', 'pdf']);
            $table->integer('points')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('chapters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('content')->nullable();
            $table->integer('book_id')->unasigned()->index();
            $table->integer('position')->default()->default(0);
            $table->integer('points')->default()->default(0);
            $table->enum('visibility', ['published', 'unpublished'])->default('unpublished');
            $table->enum('type', ['book', 'pdf'])->default('book');
            $table->enum('status', ['active', 'deleted'])->default('active');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('book_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('book_id')->unasigned()->index();
            $table->integer('category_id')->unasigned()->index();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
        Schema::dropIfExists('chapters');
        Schema::dropIfExists('book_category');
    }
}
