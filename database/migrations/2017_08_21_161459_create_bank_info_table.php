<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        
        Schema::create('bank_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unasigned()->index();
            $table->integer('bank_id')->unasigned()->index();
            $table->string('account_number')->nullable();
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('contact_number')->nullable();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_info');
    }
}
