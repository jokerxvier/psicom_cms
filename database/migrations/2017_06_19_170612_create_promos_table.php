<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        Schema::create('promos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longText('description');
            $table->integer('points')->default(0);
            $table->integer('membership_id')->unasigned()->index();
            $table->enum('status', ['active', 'disabled', 'expired', 'trash'])->default('disabled');

            $table->string('image_path')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->timestamps();
        });

        Schema::create('promo_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('promo_id')->unasigned()->index();
            $table->integer('category_id')->unasigned()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promos');
        Schema::dropIfExists('promo_category');
    }
}
