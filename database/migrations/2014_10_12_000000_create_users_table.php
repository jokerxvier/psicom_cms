<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('username')->unique();
            $table->longText('about')->nullable();
            $table->string('password')->nullable();
            $table->date('birthday')->nullable();
            $table->enum('gender', ['M', 'F'])->nullable();
            $table->string('location')->nullable();
            $table->string('profile_pic', 255)->nullable();
            $table->string('social_id')->nullable();
            $table->string('social_token')->nullable();
            $table->enum('account_type', ['author', 'admin', 'default', 'ban'])->default('default');
            $table->enum('social_type', ['facebook', 'google', 'default'])->nullable();
            $table->integer('points')->default(0);
            $table->rememberToken();
            $table->enum('set_author_name', ['name', 'username'])->default('name');
            $table->enum('user_tags', ['elite', 'normal'])->default('normal');
            $table->enum('status', ['active', 'ban', 'trash'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
