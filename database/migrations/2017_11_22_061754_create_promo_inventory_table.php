<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromoInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promo_inventory', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('promo_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('vouchers_code')->nullable();
            $table->timestamp('claim_at')->nullable();
            $table->enum('status', ['claim', 'unclaim'])->default('unclaim');
            $table->enum('display_status', ['active', 'deleted'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promo_inventory');
    }
}
