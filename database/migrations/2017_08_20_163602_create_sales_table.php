<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->down();
        
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id')->unasigned()->index();
            $table->integer('buy_points');
            $table->integer('quantity');
            $table->integer('seller_id')->unsigned()->nullable();
            $table->integer('buyer_id')->unsigned()->nullable();
            $table->integer('receiver_id')->unsigned()->nullable();
            $table->enum('type', ['book', 'perks', 'avatar', 'chapter']);
            $table->enum('purchased_type', ['purchased', 'purchased_as_gift']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
