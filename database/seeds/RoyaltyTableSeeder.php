<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RoyaltyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('royalty')->insert(

        	[
            	'name' => 'elite',
            	'percentage' => 40,
            	'created_at' => Carbon::now(),
            	'updated_at' => Carbon::now(),
        	]

    	);

    	 DB::table('royalty')->insert(

        	[
            	'name' => 'normal',
            	'percentage' => 10,
            	'created_at' => Carbon::now(),
            	'updated_at' => Carbon::now(),
        	]

    	);


    }
}
