<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\User;


class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author = new Role();
		$author->name         = 'author';
		$author->display_name = 'Book author'; // optional
		$author->description  = 'Author of the book'; // optional
		$author->save();


		$app_user = new Role();
		$app_user->name         = 'app_user';
		$app_user->display_name = 'App User'; // optional
		$app_user->description  = 'App user'; // optional
		$app_user->save();

		$admin = new Role();
		$admin->name         = 'admin';
		$admin->display_name = 'Admin'; // optional
		$admin->description  = 'Admin User'; // optional
		$admin->save();

		if (getenv('APP_ENV') == 'local')
		{

			$admin_user = User::where('email', 'admin@admin.com')->first();
			$admin_user->attachRole($admin);
		}

		$marketing = new Role();
		$marketing->name         = 'marketing';
		$marketing->display_name = 'Marketing'; // optional
		$marketing->description  = 'Marketing user'; // optional
		$marketing->save();

		if (getenv('APP_ENV') == 'local')
		{
			$marketing_user = User::where('email', 'marketing@admin.com')->first();
			$marketing_user->attachRole($marketing);
		}

		$accounting = new Role();
		$accounting->name         = 'accounting';
		$accounting->display_name = 'Accounting'; // optional
		$accounting->description  = 'Accounting user'; // optional
		$accounting->save();

		if (getenv('APP_ENV') == 'local')
		{

			$accounting_user = User::where('email', 'accounting@admin.com')->first();
			$accounting_user->attachRole($accounting);
		}
    }
}
