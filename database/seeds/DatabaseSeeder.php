<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(CategoriesTableSeeder::class);
        $this->call(MembershipsTableSeeder::class);
        $this->call(PricesTableSeeder::class);
        $this->call(CardsTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(CardClassificationTableSeeder::class);
        $this->call(RoyaltyTableSeeder::class);

        if (getenv('APP_ENV') == 'local'){
            $this->call(UsersTableSeeder::class);
            $this->call(BooksTableSeeder::class);
            $this->call(FeaturedTableSeeder::class);
            $this->call(ChaptersTableSeeder::class);
            $this->call(LikesTableSeeder::class);
            $this->call(PromosTableSeeder::class);
            $this->call(AvatarsTableSeeder::class);
            $this->call(InventoryTableSeeder::class);
            $this->call(AdvertsTableSeeder::class);
            $this->call(CommentsTableSeeder::class);
            $this->call(PagesSeeder::class);
            $this->call(ScratchCardsTableSeeder::class);
            $this->call(NotificationTableSeeder::class);
        }

        $this->call(RolesTableSeeder::class);
        
    }
}
