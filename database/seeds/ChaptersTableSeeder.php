<?php

use Illuminate\Database\Seeder;

class ChaptersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();
      $books = App\Models\books::where('type', '=', 'book')->get();
      $pdfs = App\Models\books::where('type', '=', 'pdf')->get();

       	foreach ($books as $book)
       	{
         		foreach (range(1, 10) as $index)
         		{
  	       		DB::table('chapters')->insert([
  		            'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
  		            'content' => '<p>Lorem ipsum dolor sit amet, id habeo iudicabit definiebas pro, suas rationibus eam et, sumo accusam recteque vim id. No usu putant delectus. Mel purto viris posidonium ad. Cum errem definiebas ut, ne autem aliquid pertinacia quo. Mel ocurreret ullamcorper ex, qui oratio sensibus similique ut. Qui id erant zril, qui ne legere tacimates pertinacia. Accusata eleifend consetetur mea at.</p><p>Nec legimus legendos disputando id, solum habemus commune vel ne. Homero admodum consetetur at nam, ne dicta putent fabulas vis. Purto aeterno philosophia no vis, summo nulla latine te qui. Vim affert detraxit dissentias te, clita everti ad usu.</p><p>Et tota harum copiosae sea, ne appareat forensibus nec, vis nihil delectus fabellas cu. Duo appareat consequat at. Eius evertitur at qui, id pro esse ferri accusam, cu natum eripuit vulputate pro. Cum ei dictas regione. Vix labore malorum deterruisset ad, euismod commune ei nec, vis solet laoreet te.</p><p>Tota nobis nemore quo no, ex nobis bonorum iracundia mea. Brute liber possit at vim, duo nibh numquam mentitum ei, summo gloriatur te mea. Congue apeirian recusabo est eu, id sint detracto iudicabit quo, etiam accumsan ne has. Unum postea suscipiantur his at, usu id sanctus suscipiantur. No vim partem verear explicari, qui liber nemore tacimates ad. <br /><br />In eam malorum splendide. Ipsum indoctum euripidis ea per, facete corrumpit at sea. Ad quo commodo euripidis. Te mel novum facilis periculis, sed dolor tation omittantur an. Aeterno oblique hendrerit cu quo.</p><p>Augue graece dolorum pri ne, eruditi elaboraret temporibus ei ius, mea et dolore legimus adipisci. Cum ex enim numquam. Justo blandit te pri, ei nam libris concludaturque. In erat choro vim, sit dicta exerci te. Ex duo essent aliquip volumus, quis putant honestatis mel no. Probatus mnesarchum sed ne, in graece putent recusabo sed. Te sit libris pericula, ea populo graeci mea, nec no eruditi temporibus.</p><p>Primis tritani accusamus cum ad, natum phaedrum corrumpit eu vim. Sed id debitis periculis, argumentum reprehendunt eu sea, vix alterum blandit hendrerit no. Ex eius numquam mel. Habeo nulla has ut, ne etiam quaestio pro. Aperiam accusamus adolescens te vix, ius id saepe propriae, qui dolores phaedrum repudiandae te. Ad propriae similique his, sed at officiis consequat omittantur, eruditi aliquando et duo.</p><p>In offendit recusabo tincidunt vix. Iudico quodsi et vim, vel detracto legendos deserunt ad. Te qui probo signiferumque, et vis vocent eleifend partiendo, tation aliquam assueverit ius ex. At alii quas his.</p><p>Maiorum noluisse vituperata sea ut, at singulis abhorreant sit. Quo malis homero blandit id. Usu amet petentium delicatissimi ad, eam te mutat aperiri, perfecto quaerendum an duo. Eam impetus aliquid ne, quot vidisse ex est, eos latine impetus nostrum ut. Eos duis wisi mediocrem in.</p><p>In mel rebum posidonium. Utroque consequuntur pri in, stet cibo at sit. Eam clita putent feugiat ad, vis te modo senserit maluisset. Zril ubique expetenda ex eum.</p>',
  		            'book_id' => $book->id,
  		            'visibility' => 'published',
                  'type' => 'book',
                  'points' => rand(0, 10),
  		            'created_at' => Carbon\Carbon::now(),
  		            'updated_at' => Carbon\Carbon::now()
  		        ]);
  		      }
       	}

        foreach ($pdfs as $pdf)
        {

          DB::table('chapters')->insert([
                  'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
                  'content' => 'https://s3.amazonaws.com/kohad-test/Campus+Royalties.pdf',
                  'book_id' => $pdf->id,
                  'visibility' => 'published',
                  'type' => 'pdf',
                  'created_at' => Carbon\Carbon::now(),
                  'updated_at' => Carbon\Carbon::now()
              ]);


        }
    }
}
