<?php

use Illuminate\Database\Seeder;

class FeaturedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach(range(1,20) as $index)
    	{
        	factory(App\Models\Featured::class)->create(['book_id' => $index]);
            factory(App\Models\Featured::class, 1)->create(['promo_id' => $index]);
    	}

    }
}
