<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        


        if (getenv('APP_ENV') == 'local'){
            factory(App\User::class, 1)->create(['name' => 'Admin', 'password' => \Hash::make('psicomt3@m123'),  'email' => 'admin@admin.com', 'account_type' => 'admin']);

            factory(App\User::class, 1)->create(['name' => 'Marketing', 'password' => \Hash::make('test'),  'email' => 'marketing@admin.com', 'account_type' => 'admin']);

            factory(App\User::class, 1)->create(['name' => 'Accounting', 'password' => \Hash::make('test'),  'email' => 'accounting@admin.com', 'account_type' => 'admin']);


            factory(App\User::class, 1)->create(['account_type' => 'author']);
            factory(App\User::class, 10)->create(['account_type' => 'default']);
        }
        
    }
}
