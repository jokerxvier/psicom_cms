<?php

use Illuminate\Database\Seeder;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Pages::class, 5)->create(['page_name' => 'faq']);
        factory(App\Models\Pages::class, 1)->create(['page_name' => 'about_us']);

    }
}
