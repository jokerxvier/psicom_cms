<?php

use Illuminate\Database\Seeder;


class PromosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Promos::class, 100)->create([
                'status' => 'active',
                'end_date' => Carbon\Carbon::now()->addWeeks(4)
        ]);


        factory(App\Models\Promos::class, 10)->create([
            'status' => 'disabled',
            'end_date' => Carbon\Carbon::now()->addWeeks(4)
        ]);


        factory(App\Models\Promos::class, 5)->create([
            'status' => 'trash',
            'end_date' => Carbon\Carbon::now()->addWeeks(4)
        ]);

        factory(App\Models\Promos::class, 5)->create([
            'status' => 'expired', 
            'end_date' => Carbon\Carbon::now()->yesterday(),

        ]);


        /*factory(App\Models\Promos::class, 5)->create(
    	['start_date' => Carbon\Carbon::now(), 
    	 'end_date' => Carbon\Carbon::now()->addWeeks(1)
    	 ]);*/

        

        foreach(range(1,100) as $index)
        {
            $cat_id = App\Models\Categories::where('type', 'promo')->inRandomOrder()->first()->id;
            $promo_id = App\Models\Promos::where('status', 'active')->inRandomOrder()->first()->id;

            DB::table('promo_category')->insert([ //,
                'category_id' => $cat_id,
                'promo_id' => $promo_id,
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),

            ]);
        }
    }
}
