<?php

use Illuminate\Database\Seeder;
use App\Models\ScratchCards;

class ScratchCardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1,500) as $index)
        {
        	$card = App\Models\Cards::inRandomOrder()->first();
            

        	App\Models\ScratchCards::firstOrCreate([ //,
                'card_number' => ScratchCards::generateCardNumber(),
                'points' => $card->points,
                'card_id' => $card->id,
                'card_classification_id' => 1
            ]);

        }
    }
}
