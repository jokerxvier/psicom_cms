<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

    	factory(App\Models\Books::class, 30)->create();
        factory(App\Models\Books::class, 30)->create(['type' => 'pdf']);

        $books  = App\Models\Books::all();

        foreach($books as $book)
        {
       
        	$cat_id = App\Models\Categories::where('type', 'book')->inRandomOrder()->first()->id;
            $book_id = $book->id;

            $book_count =  DB::table('book_category')->where('book_id', $book_id)
                                                    ->where('category_id', $cat_id)
                                                    ->count();
            if ($book_count <= 0)
            {

                DB::table('book_category')->insert([ //,
                    'category_id' => $cat_id,
                    'book_id' => $book_id,
                    'created_at' => Carbon\Carbon::now(),
                    'updated_at' => Carbon\Carbon::now(),

                ]);
            }


            App\Models\Images::firstOrCreate([ //,
                'image_path' => "http://via.placeholder.com/288x450",
                'book_id' => $book_id,
                'extension' => 'jpg',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),

            ]);
        }

       
    }
}
