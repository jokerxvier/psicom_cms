<?php

use Illuminate\Database\Seeder;

class MembershipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\Models\Membership::class)->create(['title' => 'Premium+', 'type' => 'upgrade']);
        factory(App\Models\Membership::class)->create(['title' => 'Premium', 'type' => 'upgrade']);
        factory(App\Models\Membership::class)->create(['title' => 'Free', 'type' => 'free']);
    }
}
