<?php

use Illuminate\Database\Seeder;

class InventoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1,10) as $index)
        {
        	$user_id = App\User::inRandomOrder()->first()->id;
            $book_id = App\Models\Books::inRandomOrder()->first()->id;

        	App\Models\Inventory::firstOrCreate([ //,
                'book_id' => $book_id,
                'user_id' => $user_id,
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),

            ]);

        }

        foreach(range(1,10) as $index)
        {
        	$user_id = App\User::inRandomOrder()->first()->id;
            $promo_id = App\Models\Promos::inRandomOrder()->first()->id;

        	App\Models\Inventory::firstOrCreate([ //,
                'promo_id' => $promo_id,
                'user_id' => $user_id,
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),

            ]);

        }

        foreach(range(1,10) as $index)
        {
            $user_id = App\User::inRandomOrder()->first()->id;
            $avatar_id = App\Models\Avatars::inRandomOrder()->first()->id;



            App\Models\Inventory::firstOrCreate([ //,
                'avatar_id' => $avatar_id,
                'user_id' => $user_id,
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),

            ]);

        }




    }
}
