<?php

use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Banks::class, 1)->create(['name' => 'BDO', 'type' => 'bank']);
        factory(App\Models\Banks::class, 1)->create(['name' => 'BPI', 'type' => 'bank']);
        factory(App\Models\Banks::class, 1)->create(['name' => 'Cebuana', 'type' => 'remittance']);
        factory(App\Models\Banks::class, 1)->create(['name' => 'Palawan Express', 'type' => 'remittance']);
        factory(App\Models\Banks::class, 1)->create(['name' => 'Mlhuiller', 'type' => 'remittance']);
        factory(App\Models\Banks::class, 1)->create(['name' => 'LBC', 'type' => 'remittance']);

    }
}
