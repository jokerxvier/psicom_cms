<?php

use Illuminate\Database\Seeder;

class CardClassificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\CardClassification::class, 1)->create(['name' => 'Bank Release']);
        factory(App\Models\CardClassification::class, 1)->create(['name' => 'Printer Card Supplier']);
        factory(App\Models\CardClassification::class, 1)->create(['name' => 'IOS']);
    }
}
