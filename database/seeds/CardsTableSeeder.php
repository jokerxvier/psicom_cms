<?php

use Illuminate\Database\Seeder;

class CardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Cards::class, 1)->create(
        [
        	'title' => 'Top-Up 50',
        	'description' => 'Top up 50',
        	'sku' => 'top-up-50',
        	'price' => '50',
        	'points' => '50',
        	'type' => 'topup',

        ]);

        factory(App\Models\Cards::class, 1)->create(
        [
            'title' => 'Top-Up 300',
            'description' => 'Top up 300',
            'sku' => 'top-up-300',
            'price' => '300',
            'points' => '300',
            'type' => 'topup',

        ]);

        factory(App\Models\Cards::class, 1)->create(
        [
        	'title' => 'Top-Up 100',
        	'description' => 'Top up 100',
        	'sku' => 'top-up-100',
        	'price' => '100',
        	'points' => '100',
        	'type' => 'topup',

        ]);

        factory(App\Models\Cards::class, 1)->create(
        [
        	'title' => 'Top-Up 500',
        	'description' => 'Top up 500',
        	'sku' => 'top-up-500',
        	'price' => '500',
        	'points' => '500',
        	'type' => 'topup',

        ]);

    }
}
