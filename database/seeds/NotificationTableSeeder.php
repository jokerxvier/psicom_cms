<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class NotificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users  = App\User::all();

    	for($i = 0; $i <= 5; $i++)
    	{
	    	foreach($users as $user)
	    	{
	    		 DB::table('notifications')->insert([
	            	'message' => 'Lorem ipsum dolor sit amet',
	            	'user_id' => $user->id,
	            	'created_at' => Carbon::now(),
	            	'updated_at' => Carbon::now(),
	        	 ]);

	    	}
	    }

	    for($i = 0; $i <= 5; $i++)
    	{
	    	foreach($users as $user)
	    	{
	    		 DB::table('notifications')->insert([
	            	'message' => 'Lorem ipsum dolor sit amet',
	            	'user_id' => $user->id,
	            	'status' => 'read',
	            	'created_at' => Carbon::now(),
	            	'updated_at' => Carbon::now(),
	        	 ]);

	    	}
	    }

       
    }
}
