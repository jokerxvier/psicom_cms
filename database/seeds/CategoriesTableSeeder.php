<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$cats = ['Action', 'Adventure', 'ChickLit', 'Fanfiction', 'Fantasy', 'General Fanfiction', 'Historical Fiction', 'Horror', 'Humor', 'Mystery / Thriller', 'Non-Fiction', 'Paranormal', 'Poetry', 'Random', 'Romance', 'Science Fiction', 'Short Story', 'Spiritual', 'Teen Fiction', 'Vampire', 'Werewolf'];

        $promos = ['Event', 'Dining', 'Clothing'];

    	foreach ($cats as $cat)
    	{
    		factory(App\Models\Categories::class)->create(['name' => $cat, 'type' => 'book']);
    	}

        foreach ($promos as $promo)
        {
            factory(App\Models\Categories::class)->create(['name' => $promo, 'type' => 'promo']);
        }
        
    }
}
