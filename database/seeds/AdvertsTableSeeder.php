<?php

use Illuminate\Database\Seeder;

class AdvertsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        factory(App\Models\Adverts::class, 20)->create();

        $ads  = App\Models\Adverts::all();

        foreach($ads as $ad)
        {

            DB::table('advert_position')->insert([
                'advert_id' => $ad->id,
                'position' => 'homepage',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),

            ]);

            DB::table('advert_position')->insert([
                'advert_id' => $ad->id,
                'position' => 'reader_top',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),

            ]);

            DB::table('advert_position')->insert([
                'advert_id' => $ad->id,
                'position' => 'reader_bottom',
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),

            ]);



        }
       
        
    }
}
