<?php

use Illuminate\Database\Seeder;

class PricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Prices::class, 1)->create(['amount' => 1]);
    
        for($i = 1; $i <= 40; $i++)
        {
            factory(App\Models\Prices::class, 1)->create(['amount' => $i * 5]);
        }
       

    }
}
