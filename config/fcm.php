<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => 'AAAAqYVxd2I:APA91bHclmDYLRhM-LsSHjPHG9ti8fg7ey1dpj3sZLqtIW-70bC5R9lnG8FU7J8qd1NwPP0obguxF9_VFNVRiN_7alcGCnvJbthgIEvISbvB_XZu5yRrzkQ12_Oujd17oYACqkE2mXdy',
        'sender_id' => '728088278882',
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
