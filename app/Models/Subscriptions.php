<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriptions extends Model
{
    protected $table = 'subscriptions';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['user_id', 'membership_id', 'end_at'];


  	public function user() {
  		return belongsTo('App\User', 'user_id');
  	}

  	public function membership() {
  		return belongsTo('App\Models\Membership', 'updated_at');
  	}
}
