<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Chapters extends Model
{
	use SoftDeletes; 
	protected $table = 'chapters';
    protected $primaryKey = 'id';
    protected $fillable = ['title', 'content', 'book_id', 'visibility', 'type'];

    protected static function boot() {
        parent::boot();

        static::deleting(function($book) { 
            $book->inventory()->delete();
           
            
        });
    }


    
    public function book()
	{
	    return $this->belongsTo('App\Models\Books', 'book_id');
	}


	public function scopePublished($query)
	{
		return $query->where('visibility', 'published');
	}

	public function inventory()
	{
		return $this->hasMany('App\Models\ChapterInventory', 'chapter_id');
	}

	

}
