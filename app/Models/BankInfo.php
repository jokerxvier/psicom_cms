<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankInfo extends Model
{
    protected $table = 'bank_info';
    protected $primaryKey = 'id';
    protected $fillable = ['user_id', 'banks', 'account_number', 'account_name'];

    public function bank()
	{
		return $this->belongsTo('App\Models\Banks', 'bank_id');
	}

	public function user()
	{
		return $this->belongsTo('App\User', 'user_id');
	}
}
