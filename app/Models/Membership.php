<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    protected $table = 'memberships';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['title', 'amount'];

    public function user() {
  		return belongsToMany('App\User', 'subscriptions', 'membership_id', 'user_id');
  	}
}
