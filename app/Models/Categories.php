<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache, DB;

class Categories extends Model
{
     protected $table = 'categories';
     protected $cacheTime = 5;
     protected $tags = 'categories_cache';



     public function cache_category_all()
     {
     	//DB::connection()->enableQueryLog();

     	$data = Cache::tags('categories_cache')->rememberForever('categories',function() {
     		return $this->all();
     	});

     	//$logs = DB::getQueryLog();


     	return $data;
     }

     public function clear_cache()
     {
     	return Cache::tags($this->tags)->flush();
     }
}
