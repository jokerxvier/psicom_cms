<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardClassification extends Model
{
    protected $table = 'card_classification';
    protected $primaryKey = 'id';
}
