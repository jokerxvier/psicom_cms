<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChapterInventory extends Model
{
    protected $table = 'chapter_inventory';
    protected $primaryKey = 'id';
}
