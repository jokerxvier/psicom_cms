<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Image;

class Images extends Model
{	




    public static function resizeimage($image,  $image_size)
    {
        $image_normal = Image::make($image);

        if ($image_size['width'] != null && $image_size['height'] != null)
        {
           $image_normal = $image_normal->resize($image_size['width'],  $image_size['height']  , function ($constraint) {
                 $constraint->upsize();
            });

        }
        //$image_thumb = Image::make($image)->resize($image_size['width'],$image_size['height']);

        $output = $image_normal->stream();

        return $output;

    }

    public static function noResizeimage($image,  $image_size)
    {
        $image_normal = Image::make($image)->resize($image_size['width'],  $image_size['height'], function ($constraint) {
            $constraint->upsize();
        });

        //$image_thumb = Image::make($image)->resize($image_size['width'],$image_size['height']);

        $output = $image_normal->stream();

        return $output;

    }


    public static function uploadAws($path, $image)
    {
        Storage::disk('s3')->put($path, $image->__toString(), 'public');

        return Storage::disk('s3')->url($path);
    }

    public function upload($image)
    {

    }


}
