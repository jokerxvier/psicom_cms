<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ScratchCards extends Model
{
    protected $table = 'scratch_cards';
    protected $primaryKey = 'id';

    public static function generateCardNumber()
    {
        do {
            $num_str = str_pad(mt_rand(0, 9999999999), 11, '0', STR_PAD_LEFT);
        } // Already in the DB? Fail. Try again
        while (self::keyExists($num_str));

        return $num_str;
    }


    private static function keyExists($key)
    {
        $card_number = self::where('card_number', '=', $key)->limit(1)->count();

        if ($card_number > 0) return true;

        return false;
    }
}
