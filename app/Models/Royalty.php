<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Royalty extends Model
{
    protected $table = 'royalty';
    protected $primaryKey = 'id';
}
