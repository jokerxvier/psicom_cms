<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Books extends Model
{
	use SearchableTrait, SoftDeletes;

    protected $table = 'books';
    protected $primaryKey = 'id';
    protected $appends = ['cover'];
    protected $dates = ['release_date'];
    public static $default_cover = "https://s3.amazonaws.com/psicom/defaul_cover.png";


    protected $searchable = [

        'columns' => [
            'books.title' => 10,
        ]
       
    ];

    // this is a recommended way to declare event handlers
    protected static function boot() {
        parent::boot();

        static::deleting(function($book) { 
            $book->inventory()->delete();
            $book->images()->delete();
            $book->views()->delete();
            $book->featured()->delete();
        });
    }

    public function images()
	{
		return $this->hasMany('App\Models\Images', 'book_id');
	}

	public function chapters()
	{
		return $this->hasMany('App\Models\Chapters', 'book_id');
	}

	public function categories()
	{
	    return $this->belongsToMany('App\Models\Categories', 'book_category', 'book_id', 'category_id');
	}

	public function getCoverAttribute()
	{
		if ($this->images()->count() > 0)
		{
			return $this->images()->first()->image_path;
		}

		return $this->default_cover;
	}

	public function inventory()
	{
	    return $this->belongsToMany('App\User', 'inventory', 'book_id', 'user_id');
	}

	public function comments()
	{
	    return $this->belongsToMany('App\User', 'comments', 'book_id', 'user_id');
	}



	public function views()
	{
	    return $this->belongsToMany('App\User', 'views', 'book_id', 'user_id');
	}

	public function featured()
    {
    	return $this->hasOne('App\Models\Featured', 'book_id');
    }




}
