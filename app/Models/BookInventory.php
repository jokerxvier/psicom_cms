<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookInventory extends Model
{
    protected $table = 'book_inventory';

    public function book()
	{
	    return $this->belongsTo('App\Models\Books', 'book_id');
	}

	public function user()
	{
	    return $this->belongsTo('App\User',  'user_id');
	}
}
