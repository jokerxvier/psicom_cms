<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;


class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public $default_pic = "https://s3.amazonaws.com/psicom/default.png";
    protected $appends = ['author_name'];

    protected $fillable = [
        'name', 
        'email', 
        'username',
        'password', 
        'social_id', 
        'social_token', 
        'account_type',
        'social_type',
        'points', 
        'remember_token',
        'gender',
        'location',
        'birthday',
        'profile_pic',
        'set_author_name',
        'user_tags',
    ];

    public  $registration_rules = [
        'name' => 'required|string|max:255',
        'email' => 'required|email|max:255|unique:users',
        'username' => 'required|alpha_dash|unique:users,username',
        'password' => 'required|min:6',
        'password_confirm' => 'required|same:password',
        
    ];

    public  $update_user_rules =  [
        'name'  => 'sometimes|string|max:255',
        'birthday' => 'sometimes|date|date_format:Y-m-d',
        'location' => 'sometimes|max:255'
    ];

    public $forgot_password_rules = [
        'email' => 'required|email|max:255|exists:users,email',
        'password' => 'required',
        'password_confirm' => 'required|same:password',
        'reset_token' => 'required'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function bank_info()
    {
        return $this->hasOne('App\Models\BankInfo', 'user_id');
    }

    public function device_token()
    {
        return $this->hasMany('App\Models\Devicetokens', 'user_id');
    }

    public function followers()
    {
        return $this->belongsToMany('App\User', 'followers', 'follower_id', 'user_id')->withTimestamps();
    }

    public function following()
    {
        return $this->belongsToMany('App\User', 'followers', 'user_id', 'follower_id')->withTimestamps();
    }


    public function setPasswordAttribute($password)
    {   

        if( strlen($password) == 60 && preg_match('/^\$2y\$/', $password ))
        {
           $this->attributes['password'] =  $password;
        }else {
           $this->attributes['password'] =  Hash::make($password);
        }
        
    }

    public function setGenderAttribute($gender)
    {   
   

        switch ($gender) {
            case 'M':
                    $this->attributes['gender'] = 'M';
                break;

            case 'F':
                    $this->attributes['gender'] = 'F';
                break;
            
            default:
                     $this->attributes['gender'] = null;
                break;
        }
    }

    public function getProfilePicAttribute($profile_pic)
    {

        return ($profile_pic)  ? $profile_pic : $this->default_pic; 
    }

    public function getAccountTypeAttribute($type)
    {
        switch ($type) {
            case 'author':
                  return "Author";
                break;
            case 'admin' :
                    return "Admin";
                break;
            
            default:
                    return "Reader";
                break;
        }
    }

    public function getAuthorNameAttribute()
    {
        if ($this->set_author_name == 'username')
        {
            return $this->username;
        }else {
            return $this->name;
        }
    }





}
