<?php

namespace App\Http\Controllers\Bank;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banks;
use App\Models\BankInfo;
use Redirect, Validator, Auth;

class RemittanceController extends Controller
{
    public  $rules = [
        'bank' => 'required|exists:banks,id',
        'name' => 'required|max:255',
        'address' => 'required|max:255',
        'contact_number' => 'required|max:255',
    ];

    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index()
    {

    	$user = Auth::user();
    	$banks = Banks::where('type', 'remittance')->pluck('name', 'id');
   
    	return view('bank.remittance', compact('banks', 'user'));
    }

    public function create(Request $request)
    {
    	$user = Auth::user();

    	$params = $request->only('name', 'address', 'bank', 'contact_number');

    	$validator = Validator::make($params, $this->rules);

    	if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::back()->withErrors($validator)->withInput($request->except('password'));
        }

        $info = BankInfo::where('user_id', $user->id);

        if ($info->count() > 0)
        {
        	$user_info  = $info->first();
        }else {
        	$user_info  = new BankInfo;
        }

        $user_info->bank_id = $params['bank'];
        $user_info->name = $params['name'];
        $user_info->address = $params['address'];
        $user_info->contact_number = $params['contact_number'];
        $user_info->user_id = $user->id;
        $user_info->save();

        return Redirect::route('profile', '#bankInfo');
    }

    public function delete($bank_info_id)
    {
        $bank = BankInfo::find($bank_info_id);

        if (!$bank) { abort(404, 'Not Found'); }

        $bank->delete();

        return Redirect::route('profile', '#bankInfo');


    }
}
