<?php

namespace App\Http\Controllers\Bank;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Banks;
use App\Models\BankInfo;
use Redirect, Validator, Auth;

class BankController extends Controller
{

	public  $rules = [
        'bank' => 'required|exists:banks,id',
        'account_number' => 'required|max:255',
        'account_name' => 'required|max:255'
    ];

    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function index(Request $request)
    {
    	$user = Auth::user();
    	$banks = Banks::where('type', 'bank')->pluck('name', 'id');
   
    	return view('bank.index', compact('banks', 'user'));
    }

    public function create(Request $request)
    {
    	$user = Auth::user();

    	$params = $request->only('account_name', 'account_number', 'bank');

    	$validator = Validator::make($params, $this->rules);

    	if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::back()->withErrors($validator)->withInput($request->except('password'));
        }

        $info = BankInfo::where('user_id', $user->id);

        if ($info->count() > 0)
        {
        	$user_info  = $info->first();
        }else {
        	$user_info  = new BankInfo;
        }

        $user_info->bank_id = $params['bank'];
        $user_info->name = $params['account_name'];
        $user_info->account_number = $params['account_number'];
        $user_info->user_id = $user->id;
        $user_info->save();

        return Redirect::route('profile', '#bankInfo');
    }

    public function delete($bank_info_id)
    {
        $bank = BankInfo::find($bank_info_id);

        if (!$bank) { abort(404, 'Not Found'); }

        $bank->delete();

        return Redirect::route('profile', '#bankInfo');
    }
}
