<?php

namespace App\Http\Controllers\Pdf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Books;
use App\Models\Chapters;
use Illuminate\Support\Facades\Storage;
use Redirect, Validator, Auth, Response;

class BookPdfController extends Controller
{
	protected $books;
	protected $chapters;

	 protected $rules = [
        'pdfFile' => 'sometimes|required|mimes:pdf',
        'amount' => 'required|integer|min:0|max:200',
    ];

    public function __construct(Books $books, Chapters $chapters)
    {
    	$this->middleware('auth');
    	$this->middleware('user.book');

    	$this->books = $books;
    	$this->chapters = $chapters;

    }

    public function index($book_id)
    {
    	$book = $this->books->find($book_id);

        if ($book->type == 'book') { abort(404); }

    	$chapters = $this->chapters->where('book_id', $book_id)->get();

        if (!$book){abort(404);}

        return view('book.pdf', compact('book', 'chapters'));
    }

    public function edit($book_id, $chapter_id)
    {
        
        $chapters = $this->chapters->find($chapter_id);

        if (!$chapters) { abort(404); }

        $book = $this->books->find($chapters->book_id);

        if (!$book){abort(404);}

        if ($book->type == 'book') { abort(404); }


        return view('book.pdf_edit', compact('book', 'chapters'));
    }

    public function create($book_id, Request $request)
    {
    	$validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::back()->withErrors($validator);
        }

        $book = $this->books->find($book_id);

         if (!$book){abort(404);}

        $file = $request->file('pdfFile');
        $file_name = 'pdf_' . $book->id  . '_' . time() . '_'  . Auth::user()->id . '.' . $file->getClientOriginalExtension();

        $path = 'book_pdf/' . $file_name;

       	Storage::disk('s3')->put($path, file_get_contents($file));

       	$url = Storage::disk('s3')->url($path);

       	$book->chapters()->delete();

       	$chapter = new Chapters;
       	$chapter->title = $book->title;
       	$chapter->book_id = $book->id;
        $chapter->points = $request->amount;
       	$chapter->type = 'pdf';
       	$chapter->content = $url;

       	$chapter->save();

       	$request->session()->flash('alert-success', 'Upload Success');

        return Redirect::back();


        //return Storage::disk('s3')->url($path);
    }

    public function update($book_id, $chapter_id, Request $request)
    {
        $url = false;

        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::back()->withErrors($validator);
        }

        $book = $this->books->find($book_id);

        if (!$book){abort(404);}

        if ($request->hasFile('pdfFile'))
        {

            $file = $request->file('pdfFile');
            $file_name = 'pdf_' . $book->id  . '_' . time() . '_'  . Auth::user()->id . '.' . $file->getClientOriginalExtension();

            $path = 'book_pdf/' . $file_name;

            Storage::disk('s3')->put($path, file_get_contents($file));

            $url = Storage::disk('s3')->url($path);

        }

      
        $chapter = Chapters::find($chapter_id);
        $chapter->points = $request->amount;

        if ($url)
        {
            $chapter->content = $url;
        }
        
        $chapter->save();

        $request->session()->flash('alert-success', 'Update Success');

        return Redirect::route('book.pdf', $book->id);

    }




    public function publish($book_id, $chapter_id, Request $request)
    {


        $chapter =  $this->chapters->find($chapter_id);

        if (!$chapter)
        {
           return Response::json(array('success' => false, 'errors' => 'not_found', 'message' => 'Chapter not found.'));
        }

        if ($chapter->visibility == 'unpublished')
        {
            $chapter->visibility = 'published';
        }else {
            $chapter->visibility = 'unpublished';
        }

        $chapter->save();

        $request->session()->flash('alert-success', 'Chapters has been publish');

        return Redirect::back();
    }

    public function delete($book_id, $chapter_id, Request $request)
    {
        $book = Books::find($book_id);

        if (!$book){ abort(404); }

        
        $chapter = $this->chapters->find($chapter_id);

        if (!$chapter)
        {
            abort(404);
        }

        if ($chapter->inventory()->count() > 0)
        {
            $request->session()->flash('alert-danger', 'Chapter cannot be deleted because there are already paid users');

            return Redirect::back();

        }

        $chapter->delete();

        $request->session()->flash('alert-success', 'Delete Success');

        return Redirect::back();

    }
}
