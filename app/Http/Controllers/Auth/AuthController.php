<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Redirect, Validate;

class AuthController extends Controller
{
    public function __construct()
    {
         $this->middleware('guest')->except('logout');
    }


    public function authenticate(Request $request)
    {

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'account_type' => 'admin'])) 
        {
            // Authentication passed...
            return redirect()->intended('home');
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'account_type' => 'author']))
        {
            // Authentication passed...
            //return redirect()->intended('home');

            return redirect()->intended('home');
        }
        	
        return Redirect::back()->withErrors(['email' => 'These credentials do not match our records.'])->withInput();
         
    }
}
