<?php

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator, Redirect;
use App\Models\Role;
use App\Models\Invite;
use Carbon\Carbon;

class RegisterController extends Controller
{

    protected $users;
    protected $role;
	
    public function __construct(User $user, Role $role)
    {
    	$this->middleware('guest');
        $this->users = $user;
        $this->role = $role;
    }

    public function index(Request $request)
    {
        $token = $request->has('token') ? $request->input('token') : false;

        if (!$token)
        {
             abort(404, 'Token Required');
        }
        // Look up the invite
        if (!$invite = Invite::where('token', $token)->where('status', 'pending')->first()) {
            //if the invite doesn't exist do something more graceful than this
            abort(404, 'Invalid Token');
        }

        if ($invite->valid_till < Carbon::now()) {

            $invite->delete();
            abort(404, 'Token Expired');
          
        } 


    	return view('auth.register', compact('token'));
    }

    public function register(Request $request)
    {
        $params = $request->only(['name', 'username', 'email', 'password']);

        if (!$invite = Invite::where('token', $request->input('token'))->where('status', 'pending')->first()) {
            $invite->delete();
            abort(404, 'Invalid Token');
        }


        $validator = Validator::make($request->all(), $this->users->registration_rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::back()->withErrors($validator)->withInput($request->except('password'));
        }

        $author_role = $this->role->where('name', 'author')->first();

        $user_data = array_merge($params, ['account_type' => 'author', 'social_type' => 'default', 'profile_pic' => $this->users->default_pic, 'user_tags' => env('USER_TAG', 'normal')]);

        $user = $this->users::create($user_data);

        $user->attachRole($author_role); //assign role

        if (!$user)
        {
            abort(404, 'Query Failed.');
        }

        $invite->delete();

        $request->session()->flash('alert-success', 'User was successful added!');

        return Redirect::route('login');

    }
}
