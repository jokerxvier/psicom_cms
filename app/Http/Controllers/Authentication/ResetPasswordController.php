<?php

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use App\Http\Controllers\Controller;
use App\User;
use Validator, Redirect, DB;

class ResetPasswordController extends Controller
{
	protected $users;
	protected $hash;


    public function __construct(User $user,  HasherContract $hash)
    {
    	$this->users = $user;
    	$this->hash = $hash;

    }

    public function reset(Request $request)
    {


            $user = User::where('email', $request->input('email'))->first();

              $token = $this->broker()->createToken($user);
    	$validator = Validator::make($request->all(), $this->users->forgot_password_rules);


        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::back()->withErrors($validator);
        }

        
        if (!$is_valid = $this->validateToken($request->email, $request->reset_token))
        {
        	$request->session()->flash('alert-danger', 'Invalid Token');
        	return Redirect::route('login');
        }
        

		$user_data = $this->users->where('email', $request->email);


        if ($user_data->count() <= 0)
        {
        	abort(403, 'Query Failed.');
        }

       
        DB::table('users')
            ->where('email', $request->email)
            ->update(['password' => bcrypt($request->password)]);

        $request->session()->flash('alert-success', 'You have successfully change your password');
        return Redirect::route('login');


    }


    private function validateToken($email, $token)
    {
    	$user_token = DB::table('password_resets')->where('email', $email)->first();

        if ($this->hash->check($token, $user_token->token))
		{
			return true;
		}

        return false;
    }
}
