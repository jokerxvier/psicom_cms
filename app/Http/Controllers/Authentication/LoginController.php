<?php

namespace App\Http\Controllers\Authentication;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth, Redirect, Validate;

class LoginController extends Controller
{
	
    public function __construct()
    {
    	$this->middleware('guest')->except('logout');
        
    }

    public function index()
    {
    	return view('auth.login');
    }

    public function authenticate(Request $request)
    {


    	$params = $request->only(['email', 'password']);

        //if (Auth::attempt(['email' => $params['email'], 'password' => $params['password'], 'account_type' => 'author']))

    	if (Auth::attempt(['email' => $params['email'], 'password' => $params['password'], 'account_type' => 'author', 'status' => 'active']))
        {
            return redirect()->intended('home');


        }else if (Auth::attempt(['email' => $params['email'], 'password' => $params['password'], 'account_type' => 'admin', 'status' => 'active'])) {

             return redirect()->intended('home');


        }else if (Auth::attempt(['email' => $params['email'], 'password' => $params['password'],  'status' => 'ban'])) {

            Auth::logout();
            return Redirect::back()->withErrors(['email' => 'Account has been block.'])->withInput();
        }else {


        	return Redirect::back()->withErrors(['email' => 'These credentials do not match our records.'])->withInput();
        }

    }

    public function logout()
    {
    	Auth::logout();
		return Redirect::route('login');
    }
}
