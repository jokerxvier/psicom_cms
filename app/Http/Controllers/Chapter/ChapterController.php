<?php

namespace App\Http\Controllers\Chapter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Books;
use App\Models\Chapters;
use App\Models\Images;
use App\Jobs\Chapter\PublishChapter;
use Validator, Response, Input, Auth, Redirect;



class ChapterController extends Controller
{
	protected $books;
    protected $chapters;
    protected $path = 'chapter';
    protected $rules = [
        'title' => 'required|max:80',
        'content' => 'required'
    ];

    protected $image_rules = [
        'file' => 'image |  max:1000'
    ];

    protected $image_size = [
        'width' => null,
        'height' => null
    ];

    protected $default_title = "Untitled";

    public function __construct(Books $books, Chapters $chapters)
    {
    	$this->middleware('auth');
    	$this->middleware('user.book');
        $this->books = $books;
        $this->chapters = $chapters;
    }

    public function index($book_id)
    {

    	$book = $this->books->find($book_id);

        if ($book->type == 'pdf') { abort(404); }

        if (!$book){abort(404);}

        $chapters = $this->chapters->where('book_id', $book_id)->orderBy('position', 'asc')->orderBy('created_at', 'asc')->get();

    	return view('book.chapter', compact('book', 'chapters'));
    }

    public function create($book_id, Request $request)
    {


    	$book = $this->books->find($book_id);
        $chapter_count = $this->chapters->where('book_id', $book_id)->count() + 1;
        $title = $this->default_title . $chapter_count;

        Chapters::increment('position');

        $chapter = new Chapters;
        $chapter->title = $title;
        $chapter->book_id = $book->id;
        $chapter->position = $chapter_count;
        $chapter->save();


        if ($chapter) {
              
            return redirect()->route('chapter.edit', [$book_id, $chapter->id]);
        }

        return $redirect()->back();
    }

    public function edit($book_id, $chapter_id)
    {
        $book = $this->books->find($book_id);
        $chapter = $this->chapters->find($chapter_id);
        return view('book.chapter_create', compact('chapter', 'book'));
    }

    public function update($book_id, $chapter_id, Request $request)
    {
        $rules = [
            'title' => 'required|max:200',
            'content' => 'required',
            'amount' => 'required|integer|min:0|max:200',

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Response::json(array('success' => false, 'error' => 'validation_error', 'message' => $validator->errors()->all()));
        }
        
        $chapter = $this->chapters->find($chapter_id);

        if (!$chapter)
        {
           return Response::json(array('success' => false, 'errors' => 'not_found', 'message' => 'Chapter not found.'));
        }

        $chapter->title = $request->title;
        $chapter->content = $request->content;
        $chapter->points = $request->amount;
        $chapter->save();

        

        return Response::json(array('success' => true, 'errors' => '', 'message' => 'Post was updated.'));
    }

    public function sortable($book_id, Request $request)
    {
        

        if($request->has('chapter'))
        {
            $i = 0;
            foreach($request->input('chapter') as $id)
            {
                $i++;
                $chapter = Chapters::find($id);
                $chapter->position = $i;
                $chapter->save();
            }   

            return Response::json(array('success' => true));
        }
        else
        {
            return Response::json(array('success' => false));
        }
    }

    public function publish($book_id, $chapter_id, Request $request)
    {

        $rules = [
            'title' => 'required|max:200',
            'content' => 'required',
            'amount' => 'required|integer|min:0|max:200',

        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Response::json(array('success' => false, 'error' => 'validation_error', 'message' => $validator->errors()->all()));
        }

        $chapter =  $this->chapters->find($chapter_id);

        if (!$chapter)
        {
           return Response::json(array('success' => false, 'error' => 'not_found', 'message' => 'Chapter not found.'));
        }


        if ($chapter->visibility == 'unpublished')
        {
            $chapter->visibility = 'published';

            $this->dispatch(new PublishChapter(Auth::user(), $chapter));
        }else {
            $chapter->visibility = 'unpublished';
        }

        $chapter->title = $request->title;
        $chapter->content = $request->content;
        $chapter->points = $request->amount;
        $chapter->save();

        return Response::json(array('success' => true, 'errors' => '', 'message' => 'Post was updated.'));
    }

    public function delete($book_id, $chapter_id, Request $request)
    {
        $book = Books::find($book_id);

        if (!$book){ abort(404); }

        
        $chapter = $this->chapters->find($chapter_id);

        if (!$chapter)
        {
            abort(404);
        }

        if ($chapter->inventory()->count() > 0)
        {
            $request->session()->flash('alert-danger', 'Chapter cannot be deleted because there are already paid users');

            return Redirect::back();
        }


        $chapter->delete();

        $chapters = $this->chapters->where('book_id', $book_id);
        
        if ($chapters->count() <= 1)
        {
        
            $book->visibility = 'unpublished';

            $book->save();
        }

        $request->session()->flash('alert-success', 'Delete Success');

        return Redirect::back();

    }

    public function upload($book_id, Request $request)
    {




        /*$validator = Validator::make($request->all(), $this->image_rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Response::json(array('success' => false, 'errors' => $validator->getMessageBag()->toArray()));
        }*/
    
        if($request->hasFile('files')) 
        {


            $res = [];

            $file = $request->file('files')[0];

            $image_name = time() . '_'  . Auth::user()->id . '.' . $file->getClientOriginalExtension();
            $image = Images::resizeimage($file, $this->image_size);

            $destination = $this->path . '/' . $image_name;

            $url = Images::uploadAws( $destination, $image);

             $res['files'][] = ['url' => $url];  

            return Response::json($res);
        }

       



    }
}
