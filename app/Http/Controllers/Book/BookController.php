<?php

namespace App\Http\Controllers\Book;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\Membership;
use App\Models\Books;
use App\Models\Prices;
use App\Models\Images;
use App\Models\Chapters;
use Input, Validator, Redirect, Carbon\Carbon, Auth, Response;
use App\Jobs\Book\PublishNotification;

class BookController extends Controller
{
    protected $categories;
    protected $path = 'book_cover';
    protected $books;
    protected $limit = 10;
    protected $membership_type_free = 3;

    protected $cover_size = [
        'width' => 288,
        'height' => 450
    ];

    public  $rules = [
        'title' => 'required|max:80',
        'description' => 'required|max:2000',
        'ratings' => 'in:mature,general',
        'visibility' => 'in:published,unpublished',
        'category' => 'required|exists:categories,id',
        'points' => 'sometimes|integer|exists:prices,id',
        'release_date' => 'required|date',
        'cover' => 'image|mimes:jpeg,jpg,png|max:1000|dimensions:width=288,height=450',
        'terms' => 'sometimes|required',
        'type' => 'sometimes:required|in:pdf,book'

    ];

    public function __construct(Categories $categories, Books $books)
    {
        $this->middleware('auth');
        $this->middleware('user.book', ['only' => ['edit', 'update']]);

        $this->categories = $categories;
        $this->books = $books;


    }

    public function index(Request $request)
    {
        $params = $request->has('s') ?  $request->input('s') : false;

        $books = $this->books;

        $books = $books->where('author_id', Auth::user()->id)
                            ->where('visibility', '!=', 'deleted');

        if ($params)
        {
            $books = $books->search($params);

        }else {
            $books = $books->orderBy('created_at', 'DESC');
         
        }
        

          $books = $books->paginate($this->limit);
        
        return view('book.index', compact('books'));
    }

    public function create()
    {
        
        //$categories = $this->categories->cache_category_all();
        $points = Prices::pluck('amount', 'id');
        $membership = Membership::pluck('title', 'id');
        $categories = $this->categories->where('type', 'book')->orderBy('name', 'asc')->pluck('name', 'id');

    	return view('book.create', compact('categories', 'membership', 'points'));
    }

    public function insert(Request $request)
    {
  
        $url = false;
        $image = false;

        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::back()->withErrors($validator)->withInput($request->except('password'));
        }

        if($request->hasFile('cover')){

            $cover = $request->file('cover');

            $image_name =  time() . '_'  . Auth::user()->id . '.' . $cover->getClientOriginalExtension();

            $image = Images::resizeimage($cover, $this->cover_size);

            $destination = $this->path . '/' . $image_name;

            $url = Images::uploadAws( $destination, $image);

            $image = new Images;
            $image->extension = $cover->getClientOriginalExtension();
            $image->image_path = $url;
        }else {
            $image = new Images;
            $image->extension = 'png';
            $image->image_path = $this->books::$default_cover;
        }

        $books = new Books;
        $books->title = $request->title;
        $books->description = $request->description;

        if ($request->has('ratings') && $request->ratings != null)
        {
            $books->ratings = $request->ratings;
        }

        if ($request->has('visibility') && $request->visibility != null)
        {
            $books->visibility = $request->visibility;
        }

        
        $books->type =  $request->type;
        $books->release_date = Carbon::parse($request->release_date)->format('Y-m-d');
        $books->membership_type = $this->membership_type_free;//set as free;
        $books->points = 0;
        $books->author_id = Auth::user()->id;
        $books->save();

        //$cat = Categories::find($request->category);
        $books->categories()->attach($request->category);

        if ($image && $books)
        {
            $books->images()->save($image);
        }


        $request->session()->flash('alert-success', 'Success Adding new Book!');

        if ($request->type == 'pdf')
        {
            return Redirect::route('book.pdf', $books->id);
        }else {
            return Redirect::route('chapter', $books->id);
        }


        
       
    }

    public function edit($book_id)
    {

        $book = $this->books->find($book_id);
        $book_category_id = ($book->categories->count() > 0)  ? $book->categories->pluck('id') : null;


        $points = Prices::pluck('amount', 'id');
        $membership = Membership::pluck('title', 'id');
        $categories = $this->categories->where('type', 'book')->orderBy('name', 'asc')->pluck('name', 'id');

        return view('book.edit', compact('book', 'categories', 'membership', 'points', 'book_category_id'));
    }

    public function update($book_id, Request $request)
    {
        $cover_url = false;
        $cover = false;
        $params = $request->except('token', 'points');

        $validator = Validator::make($params, $this->rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::back()->withInput($request->input())->withErrors($validator);
        }

        $books = $this->books->find($book_id);

        if (!$books)
        {
            return Redirect::back();
        }

        if ($params['visibility'] == 'published')
        {
            $chapters = Chapters::where('book_id', $books->id)->where('visibility', 'published');

            if ($chapters->count() <= 0)
            {
                return Redirect::back()->withErrors(['At least 1 published chapter to publish this books']);
                exit;
            }
        }


        if($request->hasFile('cover')){
            $cover = $request->file('cover');
            $cover_url = $this->upload_cover($cover);

            if ($books->images->count() > 0)
            {
                $image_id = $books->images->first()->id;

                $image = Images::find($image_id);
                $image->extension = $cover->getClientOriginalExtension();
                $image->image_path = $cover_url;
                $image->save();
                
            }else {

                $image = new Images;
                $image->extension = $cover->getClientOriginalExtension();
                $image->image_path = $cover_url;
                $books->images()->save($image);
            }
        }

        if ($books->visibility == 'unpublished' && $request->visibility == 'published')
        {
            $this->dispatch(new PublishNotification(Auth::user(), $books));
        }

    
  
        $books->title = $request->title;
        $books->description = $request->description;
        $books->ratings = $request->ratings;
        $books->visibility = $request->visibility;
        $books->release_date = Carbon::parse($request->release_date)->format('Y-m-d');
        $books->membership_type = $this->membership_type_free;
    
        $books->points  = 0;
     
        $books->save();

        //$cat = Categories::find($request->category);
        $books->categories()->sync($request->category);

        $request->session()->flash('alert-success', 'Update Success');

        return Redirect::back();
    }

    public function change_visibility(Request $request)
    {
        $book_id = $request->input('book_id');

        $books = $this->books->with('chapters')->where('id', $book_id);

        
        if ($books->count() <= 0)
        {
            return Redirect::back();
        }


        $books = $books->first();


        $chapters = Chapters::where('book_id', $books->id)->where('visibility', 'published');

        if ($chapters->count() <= 0)
        {

            return Redirect::back()->withErrors(['At least 1 published chapter to publish this books']);

            exit;
        }



        if ($books->visibility == 'published' )
        {

            $books->visibility = 'unpublished';
            $books->save();

            $request->session()->flash('alert-success', 'Update Success');

            return Redirect::back();


            exit;
        }

        if ($books->visibility == 'unpublished' )
        {

            

            $books->visibility = 'published';
            $books->save();

            $request->session()->flash('alert-success', 'Update Success');

            return Redirect::back();


            exit;
        }

    }

    public function delete($book_id)
    {
        $book = $this->books->find($book_id);

        if (!$book)
        {
            return Redirect::back();
        }

        $book->visibility = 'deleted';
        $book->save();

       return Redirect::back();

    }

    public function upload_cover($cover)
    {
        

        $image_name =  time() . '_'  . Auth::user()->id . '.' . $cover->getClientOriginalExtension();

        $image = Images::resizeimage($cover, $this->cover_size);

        $destination = $this->path . '/' . $image_name;

        $url = Images::uploadAws( $destination, $image);


        return $url;

    }


    public function search(Request $request)
    {
        $results = array();
        $params = $request->has('query') ?  $request->input('query') : '';

        $user = Auth::user();

        $books = $this->books::where('author_id', $user->id)
                    ->where('visibility', '!=', 'deleted')
                    ->search($params)
                    ->take(5)
                    ->get();

        foreach ($books as $book)
        {
            $results[] = $book->title;
        }

        return Response::json($results);

    }



}
