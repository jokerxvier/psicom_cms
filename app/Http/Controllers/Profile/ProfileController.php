<?php

namespace App\Http\Controllers\Profile;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Carbon\Carbon, Auth, Redirect, URL, Hash, Storage;
use App\Models\Images;
use App\Models\AdditionalInfo;
use App\Models\Banks;


class ProfileController extends Controller
{
	protected $user;
    protected $path = 'avatar';
    protected $avatar_rules = [
        'avatar' => 'required|image| mimes:jpeg,jpg,png|max:2000|dimensions:width=200,height=200'
    ];

    protected $avatar_size = [
        'width' => 200,
        'height' => 200
    ];

    public function __construct(User $user)
    {
    	$this->user = $user;
        $this->middleware('auth');
    }

    public function index()
    {   

    	return view('profile.index');
    }

    public function update_user_info(Request $request)
    {
        
    	$params = $request->only('name', 'birthday', 'location', 'gender', 'set_author_name');

    
    	if ($request->has('birthday'))
    	{
    		$params['birthday'] = Carbon::parse($params['birthday'])->format('Y-m-d');
    	}


    	$validator = Validator::make($params, $this->user->update_user_rules);


        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::back()->withErrors($validator);
        }

        try {

        	$user = Auth::user();
        
	        $user->update($params);

	        $request->session()->flash('alert-success', 'User was successful Updated!');


        
            return Redirect::to(URL::previous() . "#userinfo");

        }catch (Illuminate\Database\QueryException $e){
        	//return Redirect::back();
        	abort(404);
        }

    }

    public function change_password(Request $request)
    {
        
        $rules = [
            'password' => 'required|different:old_password|confirmed',
            'password_confirmation' => 'required|same:password',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            $messages = $validator->messages();



            return Redirect::to(URL::previous() . "#userPassword")->withErrors($validator);
        }

        $credentials = $request->only(
                'old_password', 'password', 'password_confirmation'
        );

        $user = $this->user->find(Auth::user()->id);

        if (Hash::check($credentials['old_password'], $user->password)) 
        {
            $user->password =   $credentials['password'];
            $user->save();

            $request->session()->flash('alert-success', 'Your new password is now set!');

            return Redirect::to(URL::previous() . "#userPassword");

        }else {
            
           $request->session()->flash('alert-danger', 'Whoops! There were some problems with your input. Your password did not match');
            return Redirect::to(URL::previous() . "#userPassword");
        }

        return Redirect::back();
        
    }


    public function upload_avatar(Request $request)
    {
        

        $validator = Validator::make($request->all(), $this->avatar_rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::to(URL::previous() . "#userAvatar")->withErrors($validator);
        }

        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');

            $image_name = time() . '_'  . Auth::user()->id . '.' . $avatar->getClientOriginalExtension();

            $image = Images::resizeimage($avatar, $this->avatar_size);

            $destination = $this->path . '/' . $image_name;

            $avatar_url = Images::uploadAws( $destination, $image);

            $user = $this->user->find(Auth::user()->id);
            $user->profile_pic = $avatar_url;
            $user->save();

            $request->session()->flash('alert-success', 'Your have uploaded new avatar!');
            return Redirect::to(URL::previous() . "#userAvatar");
        }

         return Redirect::back();

    }

    public function update_bank_info(Request $request)
    {   
        $request->request->add(['user_id' => Auth::user()->id]);
        $params = $request->only('banks', 'account_number', 'account_name', 'user_id');

        $validator = Validator::make($request->all(), ['banks' => 'in:bpi,bdo', 'account_number' => 'required|max:100', 'account_name' => 'required|max:255']);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::to(URL::previous() . "#bankInfo")->withErrors($validator);
        }

        $infos = AdditionalInfo::where('user_id', Auth::user()->id);

        if ($infos->count() > 0)
        {
             $infos->update($params);

        }else {
            AdditionalInfo::create($params);
          
        }

        $request->session()->flash('alert-success', 'User Bank Info was successful Updated!');
        return Redirect::to(URL::previous() . "#bankInfo");

    }
}
