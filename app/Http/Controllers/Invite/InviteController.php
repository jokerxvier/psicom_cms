<?php

namespace App\Http\Controllers\Invite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invite;
use App\User;
use Carbon\Carbon;
use App\Mail\InviteCreated;
use Mail, Redirect, Validator;
use Auth;

class InviteController extends Controller
{

	protected $invite;
	protected $expire = 31; //31 days = 1month

    public  $rules = [
        'email' => 'required|email'
    ];

    public function __construct(Invite $invite)
    {
    	$this->invite = $invite;
    }

    public function index() {


        if (Auth::check() && Auth::user()->account_type == 'Admin')
        {
    	   return view('invite.index');
        }else {
            abort(404, 'Only Admin can access this page.');
        }

    
    }

    public function invite(Request $request)
    {
    	$email = $request->email;

        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            return Redirect::back()->withErrors($validator)->withInput($request->except('password'));
        }
    
    	$user = User::where('email', $email);

    	if ($user->count() > 0) //Check if user exist
    	{
    		return Redirect::back()->withErrors(['User already register']);
    	}


    	$invite_data = Invite::where('email', $email)->where('status', 'pending');

    	if ($invite_data->count() > 0)
    	{
    		$invite = $invite_data->first();

    	}else {

    		do {
        
	        	$token = str_random();
	    	} //check if the token already exists and if it does, try again

	    	while (Invite::where('token', $token)->first());

	    	$invite = Invite::create([
		        'email' => $email,
		        'token' => $token,
		        'valid_till' => Carbon::now()->addDays($this->expire),
		        'status' => 'pending'
		    ]);
    	}

    
	    Mail::to($email)->send(new InviteCreated($invite));

        $request->session()->flash('alert-success', 'Invatation has been sent.');
	    return redirect()->back();

    }
}
