<?php 

function set_active($path, $active = 'active selected open') {
    return call_user_func_array('Request::is', (array)$path) ? $active : '';
}

