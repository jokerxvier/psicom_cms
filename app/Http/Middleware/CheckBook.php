<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Books;
use Auth;
use Redirect;
use Session;
class CheckBook
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::check())
        {
            $book = Books::where('author_id', Auth::user()->id)->where('id', $request->book_id);
            if ($book->count() <= 0)
            {
            
                return Redirect::route('home');

                exit;
            }
        }

        return $next($request);
    }
}
