<?php

namespace App\Jobs\Book;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Books;
use App\Models\Notifications;

class PublishNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $user;
    protected $book;

    public function __construct($user, $book)
    {
        $this->user = $user;
        $this->book = $book;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $book =  $this->book;

        $author_name = ($user->set_author_name == 'name') ? $user->name : $user->username;

         $user->followers()->chunk(100, function ($followers) use($book, $author_name) {

             foreach($followers as $follow)
             {
                Notifications::firstOrCreate(
                    [
                      'message' =>  $author_name   . ' has published a Book : ' . ucfirst($book->title), 
                      'user_id' => $follow->id
                    ]
                );

             }
        });

    }
}
