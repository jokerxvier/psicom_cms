<?php

namespace App\Jobs\Chapter;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\User;
use App\Books;
use App\Models\BookInventory;
use App\Models\Devicetokens;
use App\Models\Notifications;

class PublishChapter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $author;
    protected $chapter;

    public function __construct($user, $chapter)
    {
        $this->author = $user;
        $this->chapter = $chapter;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
      $user = $this->author;
      $chapter = $this->chapter;
    
      BookInventory::where('book_id', $chapter->book_id)->chunk(100, function ($inventories) use($chapter) {

          foreach($inventories as $inventory){ 
            Notifications::firstOrCreate(
                [
                  'message' => ucfirst($chapter->book->title) . ' has published a chapter : ' . ucfirst($chapter->title), 
                  'user_id' => $inventory->user_id
                ]
            );

          }

          
      });


    }
}
