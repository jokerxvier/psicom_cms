<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Books;
use Carbon\Carbon;

class ReleaseBook extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'release:book';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automate Releasing of books';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $now = Carbon::now()->format('Y-m-d');
        $books = Books::with('chapters')->whereHas('chapters', function ($q) {
                            $q->where('chapters.visibility', 'published');
                        })
                        ->where('visibility', 'unpublished')
                        ->whereDate('release_date', $now)
                        ->update(['visibility' => 'published']);
    }
}
