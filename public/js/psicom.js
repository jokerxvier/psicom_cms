$(function () {

	$('.formDisableOnSubmit').on('submit', function () {
		$(':input[type="submit"]').prop('disabled', true);
	    $('input[type="text"]').keyup(function() {
	        if($(this).val() != '') {
	           $(':input[type="submit"]').prop('disabled', false);
	        }
	    });
	});

	$('body').on('click', '.confirmButton', function(e){
		e.preventDefault();

	    var $form=$(this).parent('form');

	    $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .on('click', '#yes', function(e){
        	e.preventDefault();
            $form.submit();
        });
	});
});