$(function () {

	$('.termsAndCondition').on('click', function (e) {
		$('#termsModal').modal({backdrop: 'static', keyboard: false}) ;

		e.preventDefault();
	});

	$('input[type="checkbox"]').on('change', function(e){
	   if(e.target.checked){
	     $('#termsModal').modal();
	     $('input[name="terms"]').prop('checked', false);
	   }
	});

	$('#termsModal').on('show.bs.modal', function(e) {
    	$(this).find('.btn-accept').on('click', function () {
    		$('input[name="terms"]').prop('checked', true);
    		$('#termsModal').modal('hide');
    	})
	});

	var searchUrl = $('input.typeahead').data('url');

	$('input.typeahead').typeahead({
		minLength: 1,
    	maxItem: 20,
    	backdrop: {
       	 "background-color": "#fff"
    	},
    	emptyTemplate: "no result for {{query}}",
	    source:  function (query, process) {

	    	return $.get(searchUrl, { query: query }, function (res) {

	            return process(res);
	        });
	    },


	});

});