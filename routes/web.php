<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	Route::get('/', 'Book\BookController@index');
	Route::get('/home', 'Book\BookController@index')->name('home');

	//Auth::routes();



	Route::get('login', 'Authentication\LoginController@index')->name('login');
	Route::get('logout', 'Authentication\LoginController@logout')->name('logout');
	Route::get('register', 'Authentication\RegisterController@index')->name('register');



	Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset.get');
	Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

	Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset.post');



	Route::group(['prefix' => 'auth'], function () {
		Route::post('login', 'Authentication\LoginController@authenticate')->name('auth.login');
		Route::post('register', 'Authentication\RegisterController@register')->name('auth.register');
	});


	Route::group(['middleware' => 'auth'], function () {
		Route::group(['prefix' => 'author'], function () {
			Route::get('/', 'Author\AuthorController@index')->name('author');
		});

		Route::group(['prefix' => 'books'], function () {
			Route::get('/', 'Book\BookController@index')->name('books');
			Route::get('/create', 'Book\BookController@create')->name('book.create');
			Route::post('/insert', 'Book\BookController@insert')->name('book.insert');
			Route::get('/details/{book_id}', 'Book\BookController@edit')->name('book.edit');
			Route::post('/update/{book_id}', 'Book\BookController@update')->name('book.update');
			Route::get('/status', 'Book\BookController@change_visibility')->name('book.update.status');
			Route::delete('books/{book_id}','Book\BookController@delete')->name('book.delete');
			Route::get('/search', 'Book\BookController@search')->name('book.search');


			Route::group(['prefix' => 'chapters/{book_id}'], function () {
				Route::get('/', 'Chapter\ChapterController@index')->name('chapter');
				Route::get('/create', 'Chapter\ChapterController@create')->name('chapter.create');
				Route::get('/edit/{chapter_id}', 'Chapter\ChapterController@edit')->name('chapter.edit');
				Route::post('/update/{chapter_id}', 'Chapter\ChapterController@update')->name('chapter.update');
				Route::post('/delete/{chapter_id}', 'Chapter\ChapterController@delete')->name('chapter.delete');
				Route::post('/sortable', 'Chapter\ChapterController@sortable')->name('chapter.sortable');
				Route::post('/upload', 'Chapter\ChapterController@upload')->name('chapter.upload');
				Route::post('/publish/{chapter_id}', 'Chapter\ChapterController@publish')->name('chapter.publish');

			});

			Route::group(['prefix' => 'pdf/{book_id}'], function () {
				Route::get('/', 'Pdf\BookPdfController@index')->name('book.pdf');
				Route::post('/create', 'Pdf\BookPdfController@create')->name('book.pdf.create');
				Route::get('/edit/{chapter_id}', 'Pdf\BookPdfController@edit')->name('book.pdf.edit');
				Route::post('/update/{chapter_id}', 'Pdf\BookPdfController@update')->name('book.pdf.update');
				Route::post('/publish/{chapter_id}', 'Pdf\BookPdfController@publish')->name('book.pdf.publish');
				Route::post('/delete/{chapter_id}', 'Pdf\BookPdfController@delete')->name('book.pdf.delete');
			});

		});

		Route::group(['prefix' => 'profile'], function () {
			Route::get('/', 'Profile\ProfileController@index')->name('profile');
			Route::post('/', 'Profile\ProfileController@update_user_info')->name('profile.update');
			Route::post('/change-password', 'Profile\ProfileController@change_password')->name('profile.change.pass');
			Route::post('/upload-avatar', 'Profile\ProfileController@upload_avatar')->name('profile.avatar.upload');
			Route::post('/update-bank', 'Profile\ProfileController@update_bank_info')->name('profile.update.bank');

		});

		Route::group(['prefix' => 'bank'], function () {
			Route::get('/', 'Bank\BankController@index')->name('bank');
			Route::post('/save', 'Bank\BankController@create')->name('bank.save');
			Route::get('/delete/{bank_info_id}', 'Bank\BankController@delete')->name('bank.delete');
		});

		Route::group(['prefix' => 'remittance'], function () {
			Route::get('/', 'Bank\RemittanceController@index')->name('remittance');
			Route::post('/save', 'Bank\RemittanceController@create')->name('remittance.save');
			Route::get('/delete/{bank_info_id}', 'Bank\RemittanceController@delete')->name('remittance.delete');
		});


	});


	Route::group(['prefix' => 'invite'], function () {
		Route::get('/', 'Invite\InviteController@index')->name('invite')->middleware('auth');
		Route::post('/', 'Invite\InviteController@invite')->name('invite.send');
	});

	Route::group(['prefix' => 'test'], function () {
		Route::get('/', 'Test\TestController@index');
		Route::get('/push', 'Test\TestController@pushNotif');
	});


